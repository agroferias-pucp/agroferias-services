package com.agroferias.repositories;

import com.agroferias.entities.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoRepository extends JpaRepository<Estado,Integer> {
    Estado findByIdEstado(Integer idEstado);
}
