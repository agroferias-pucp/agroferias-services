package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from promociones!";
    }

    @RequestMapping(value = "/api/promociones")
    String promociones_api() {
        return "hola api promociones!";
    }

    @RequestMapping(value = "/api/promociones/a/b")
    String promociones_api_a_b() {
        return "hola api promociones a b!";
    }
}