package com.agroferias.entities;



import java.sql.Blob;
import javax.persistence.*;

@Entity
@Table(name = "Tienda")
public class Tienda {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idTienda")
	private Integer idTienda;

    @Lob
    @Column(name = "descripcion",length = 1024)	
	private String descripcion;

    @Column(name = "tipoTienda")	
	private Integer tipoTienda;

    @Column(name = "numeroPuesto")	
	private String numeroPuesto;

    @Column(name = "posicion_x")	
	private Integer posicion_x;

    @Column(name = "posicion_y")	
	private Integer posicion_y;

    @Column(name = "idSolicitudTienda")	
	private Integer idSolicitudTienda;

    @Column(name = "foto")	
	private Blob foto;

    @Column(name = "esPremium")	
	private Integer esPremium;

    @ManyToOne
    @JoinColumn(name = "idEmpresa")
	private Empresa empresa;

    @Column(name = "idFeria")	
	private Integer idFeria;

    @Column(name = "asignado")	
	private Integer asignado;

    @Column(name = "activo")	
	private Integer activo;
    


    public Tienda() {
    }

    public Tienda(Integer idTienda, String descripcion, Integer tipoTienda, String numeroPuesto, Integer posicion_x, Integer posicion_y, Integer idSolicitudTienda, Empresa empresa, Integer idFeria, Integer asignado, Integer activo, Blob foto, Integer esPremium) {
        this.idTienda = idTienda;
        this.descripcion = descripcion;
        this.tipoTienda = tipoTienda;
        this.numeroPuesto = numeroPuesto;
        this.posicion_x = posicion_x;
        this.posicion_y = posicion_y;
        this.idSolicitudTienda = idSolicitudTienda;
        this.empresa = empresa;
        this.idFeria = idFeria;
        this.asignado = asignado;
        this.activo = activo;
        this.foto = foto;
        this.esPremium = esPremium;
    }

    public Integer getIdTienda() {
        return this.idTienda;
    }

    public void setIdTienda(Integer idTienda) {
        this.idTienda = idTienda;
    }

    public Integer getEsPremium() {
        return this.esPremium;
    }

    public void setEsPremium(Integer esPremium) {
        this.esPremium = esPremium;
    }

    public Blob getFoto() {
        return foto;
    }

    public void setFoto(Blob foto) {
        this.foto = foto;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getTipoTienda() {
        return this.tipoTienda;
    }

    public void setTipoTienda(Integer tipoTienda) {
        this.tipoTienda = tipoTienda;
    }

    public String getNumeroPuesto() {
        return this.numeroPuesto;
    }

    public void setNumeroPuesto(String numeroPuesto) {
        this.numeroPuesto = numeroPuesto;
    }

    public Integer getPosicion_x() {
        return this.posicion_x;
    }

    public void setPosicion_x(Integer posicion_x) {
        this.posicion_x = posicion_x;
    }

    public Integer getPosicion_y() {
        return this.posicion_y;
    }

    public void setPosicion_y(Integer posicion_y) {
        this.posicion_y = posicion_y;
    }

    public Integer getIdSolicitudTienda() {
        return this.idSolicitudTienda;
    }

    public void setIdSolicitudTienda(Integer idSolicitudTienda) {
        this.idSolicitudTienda = idSolicitudTienda;
    }

    public Empresa getEmpresa() {
        return this.empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Integer getIdFeria() {
        return this.idFeria;
    }

    public void setIdFeria(Integer idFeria) {
        this.idFeria = idFeria;
    }

    public Integer getAsignado() {
        return this.asignado;
    }

    public void setAsignado(Integer asignado) {
        this.asignado = asignado;
    }

    public Integer getActivo() {
        return this.activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }


}