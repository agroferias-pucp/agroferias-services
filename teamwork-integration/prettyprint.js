let data = require("./test.json");
let event = JSON.parse(data.body);


console.log(JSON.stringify(event, null, 4))

/// USE CODE BELOW IN AWS LAMBDA

const https = require("https");
let re = /\[([0-9,\s]+)\]/;

function commentTask(taskId, mrDescription, mrUrl) {
	const data = JSON.stringify({
		comment: {
			body: `<a href="${mrUrl}">${mrDescription}</a>`,
			notify: "",
			isprivate: false,
			pendingFileAttachments: "",
			"content-type": "html"
		}
	});

	const options = {

		hostname: `dp22019.teamwork.com`,
		port: 443,
		method: "POST",
		path: `/tasks/${taskId}/comments.json`,
		headers: {
			"Content-Type": "application/json",
			"Content-Length": data.length,
			"Authorization": "Basic " + Buffer.from(process.env.TEAMWORK_KEY + ":xxx").toString('base64')
		}
	};

	return new Promise((ok, ko) => {
		const req = https.request(options, res => {
			console.log(`${taskId} comment: ${res.statusCode}`);
			ok();
		});
		req.on("error", error => {
			console.log(`Error setting comment for task ${taskId}`);
			console.error(error);
			ok();
		});

		req.write(data)
		req.end()
	});
}

var match = event.object_attributes.title.match(re);
if (match) {
	let taskIds = match[1].replace(" ", "").split(",");

	// console.log(match[1].replace(" ", "").split(","));
	let comments = [];
	for(let i=0;i<taskIds.length;i++) {
		let taskId = taskIds[i];
		const mrDescription = event.object_attributes.title;
		const mrUrl = event.object_attributes.url;

		comments.push(commentTask(taskId, mrDescription, mrUrl));
	}
    Promise.all(comments);
    // Return promise all
}
console.log(event.object_attributes.url);

// Return error message