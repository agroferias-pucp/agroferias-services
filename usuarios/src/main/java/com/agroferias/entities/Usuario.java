package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="Usuario")
public class Usuario{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idUsuario")
    private Integer idUsuario;

    @Column(name="idRol")
    private Integer idRol;

    @Column(name="username")
    private String username;

    @Column(name="password")
    private String password;

    @Column(name="fechaRegistro")
    private Date fechaRegistro;

    public Usuario(){};
    public Usuario(int idrol, String user, String pass, Date fechaReg){
        this.idRol=idrol;
        this.username=user;
        this.password=pass;
        this.fechaRegistro=fechaReg;
    }

    public Integer getIdUsuario(){
        return this.idUsuario;
    }
    public Integer getIdRol(){
        return this.idRol;
    }
    public String getUsername(){
        return this.username;
    }
    public String getPassword(){
        return this.password;
    }
    public Date getFechaRegistro(){
        return this.fechaRegistro;
    }

    public void setIdRol(int idRol){
        this.idRol=idRol;
    }
    public void setUsername(String user){
        this.username=user;
    }
    public void setPassword(String pass){
        this.password=pass;
    }
    public void setFechaRegistro(Date fechaReg){
        this.fechaRegistro=fechaReg;
    }

}