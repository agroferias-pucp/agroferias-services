package com.agroferias.controllers;

import com.agroferias.entities.Estado;
import com.agroferias.repositories.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/feria/estado")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})

public class EstadoController {

    @Autowired
    EstadoRepository estadoRepository;

    @RequestMapping(value = "/listarEstados", method = RequestMethod.GET)
    public List<Estado> listarEstados(){
        return estadoRepository.findAll();
    }
}
