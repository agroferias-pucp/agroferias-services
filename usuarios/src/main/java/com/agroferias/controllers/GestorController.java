package com.agroferias.controllers;

import com.agroferias.dto.AutenticationDTO;
import com.agroferias.dto.GestorDTO;
import com.agroferias.services.GestorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;


@RestController()
@RequestMapping(value = "/api/usuario/gestor")
@CrossOrigin(origins="*",methods={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class GestorController{
    @Autowired
    private GestorService servicioGestor = new GestorService();

    @RequestMapping(value="/registro",method=RequestMethod.POST)
    public GestorDTO registrarGestor(@RequestBody GestorDTO gestorRecibido){
        gestorRecibido = this.servicioGestor.registrarGestor(gestorRecibido);
        return gestorRecibido;
    }
    @RequestMapping(value="/autenticacion",method=RequestMethod.POST)
    public Integer validarGestor(@RequestBody AutenticationDTO credenciales){
        //validar usuario
        return 1;
    }
    @RequestMapping(value="/{idGestor}")
    public GestorDTO getGestorDTO(@PathVariable("idGestor") int id){
        return this.servicioGestor.getGestorDTO(id);
    }
    @RequestMapping(value="/eliminar/{idGestor}")
    public Integer eliminarGestor(@PathVariable("idGestor") int id){
        this.servicioGestor.eliminarGestor(id);
        return id;
    }
}