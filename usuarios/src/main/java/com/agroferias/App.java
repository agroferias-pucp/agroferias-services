package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    // Este método es necesario para que el servicio pueda ser detectado.
    @RequestMapping(value = "/")
    String healthcheck() {
        return "Usuarios is alive!";
    }

    @RequestMapping(value = "/api/usuarios")
    String ferias_api() {
        return "Usuarios tiene su API!";
    }

    @RequestMapping(value = "/api/usuarios/a/b")
    String ferias_api_a_b() {
        return "Usuarios ruta a/b!";
    }

}