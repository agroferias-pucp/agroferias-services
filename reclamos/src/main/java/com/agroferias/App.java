package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from reclamos!";
    }

    @RequestMapping(value = "/api/reclamos")
    String reclamos_api() {
        return "hola api reclamos!";
    }

    @RequestMapping(value = "/api/reclamos/a/b")
    String reclamos_api_a_b() {
        return "hola api reclamos a b!";
    }
}