package com.agroferias.services;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import com.agroferias.entities.Empresa;
import com.agroferias.repositories.EmpresaRepository;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaRepository empresaRepository;

    public List<Empresa> getActiveEmpresas() {
        return empresaRepository.findAllActiveEmpresa();
    }

    public Empresa servicioPerfilEmpresa(Integer idEmpresa) {
        Empresa perfilEmpresa = empresaRepository.findOne(idEmpresa);
        if (perfilEmpresa != null) {
            return perfilEmpresa;
        } else {
            return null;
        }
    }

    public Empresa registrarEmpresa(Empresa emp) {
        return empresaRepository.save(emp);
    }

    public Empresa editarEmpresa(Empresa nuevo) {
        //return empresaRepository.save(emp);
        Empresa empresa = empresaRepository.findByIdEmpresa(nuevo.getIdEmpresa());
        if (empresa != null) {
            empresa.setActivo(nuevo.getActivo());
            empresa.setRucRus(nuevo.getRucRus());
            empresa.setCelular(nuevo.getCelular());
            empresa.setDireccion(nuevo.getDireccion());
            empresa.setDireccionWeb(nuevo.getDireccionWeb());
            empresa.setEmail(nuevo.getEmail());
            empresa.setFacebook(nuevo.getFacebook());
            empresa.setFechaRegistro(nuevo.getFechaRegistro());
            empresa.setIdCategoria(nuevo.getIdCategoria());
            empresa.setIdDepartamento(nuevo.getIdDepartamento());
            empresa.setIdEmpresa(nuevo.getIdEmpresa());
            empresa.setIdSolicitudEmpresa(nuevo.getIdSolicitudEmpresa());
            empresa.setInstagram(nuevo.getInstagram());
            empresa.setNombreComercial(nuevo.getNombreComercial());
            empresa.setRazonSocial(nuevo.getRazonSocial());
            empresa.setTelefono(nuevo.getTelefono());
            empresa.setTipoPersona(nuevo.getTipoPersona());
            empresa.setTwitter(nuevo.getTwitter());;
            //empresaService.registrarEmpresa(empresa);
            return empresaRepository.save(empresa);
        }
        return null;
    }

    public Empresa buscarEmpresa(Integer id) {
        return empresaRepository.findByIdEmpresa(id);
    }

    public int eliminarEmpresa(int idEmpresa) {
        Empresa empresa = empresaRepository.findByIdEmpresa(idEmpresa);
        if (empresa != null) {
            empresa.setActivo(0);
            empresaRepository.save(empresa);
            return idEmpresa;
        } else {
            return 0;
        }
    }

    public List<Empresa> allEmpresas() {
        return empresaRepository.findAll();
    }

}
