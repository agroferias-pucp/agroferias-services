package com.agroferias.repositories;

import java.util.List;

import com.agroferias.entities.Persona;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonaRepository extends JpaRepository<Persona,Integer>{
    @Query(value="SELECT * FROM usuariosdb.Persona",nativeQuery = true)
    List<Persona> findAllPersonas();
}