package com.agroferias.entities;
public class FeriaT{

    private Integer idFeria;
    private String distrito;
    private String direccion;
    private Integer flagFisico;
    private Integer flagVirtual;
    private String tiposTienda;



    public FeriaT() {
    }

    public FeriaT(Integer idFeria, String distrito, String direccion, Integer flagFisico, Integer flagVirtual, String tiposTienda) {
        this.idFeria = idFeria;
        this.distrito = distrito;
        this.direccion = direccion;
        this.flagFisico = flagFisico;
        this.flagVirtual = flagVirtual;
        this.tiposTienda = tiposTienda;
    }

    public FeriaT(Integer idFeria, String distrito, String direccion) {
        this.idFeria = idFeria;
        this.distrito = distrito;
        this.direccion = direccion;
        this.flagFisico = 0;
        this.flagVirtual = 0;
        this.tiposTienda = "";
    }

    public Integer getIdFeria() {
        return this.idFeria;
    }

    public void setIdFeria(Integer idFeria) {
        this.idFeria = idFeria;
    }

    public String getDistrito() {
        return this.distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getFlagFisico() {
        return this.flagFisico;
    }

    public void setFlagFisico(Integer flagFisico) {
        this.flagFisico = flagFisico;
    }

    public Integer getFlagVirtual() {
        return this.flagVirtual;
    }

    public void setFlagVirtual(Integer flagVirtual) {
        this.flagVirtual = flagVirtual;
    }

    public String getTiposTienda() {
        return this.tiposTienda;
    }

    public void setTiposTienda(String tiposTienda) {
        this.tiposTienda = tiposTienda;
    }
   

}