package com.agroferias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.agroferias.services.SolicitudProductoService;
import com.agroferias.entities.SolicitudProducto;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class SolicitudProductoController {
	@Autowired
	private SolicitudProductoService solicitudProductoService;
	
	@RequestMapping(value="/api/producto/feria/solicitudes/{idFeria}")
	public List<SolicitudProducto> solicitudProductoFeria(@PathVariable(value = "idFeria") int idFeria){
		return solicitudProductoService.getSolicitudProductoFeria(idFeria);
	}
	
	@RequestMapping(value="/api/producto/tienda/solicitudes/{idTienda}")
	public List<SolicitudProducto> solicitudProductoTienda(@PathVariable(value = "idTienda") int idTienda){
		return solicitudProductoService.getSolicitudProductoTienda(idTienda);
	}

	@RequestMapping(value="/api/producto/solicitud/aceptar")
	public Integer solicitudProductoAceptar(@RequestParam int idSolicitud){
		return solicitudProductoService.aceptarSolicitudProducto(idSolicitud);
	}

	@RequestMapping(value="/api/producto/solicitud/rechazar")
	public Integer solicitudProductoRechazar(@RequestParam int idSolicitud){
		return solicitudProductoService.rechazarSolicitudProducto(idSolicitud);
	}

	@RequestMapping(value="/api/producto/solicitud/registrar")
	public @ResponseBody SolicitudProducto registrarSolicitudProducto(@RequestBody SolicitudProducto solicitudProducto) throws Exception {
		return solicitudProductoService.insert(solicitudProducto);
	}
}