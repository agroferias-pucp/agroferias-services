package com.agroferias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.agroferias.services.UnidadMedidaService;
import com.agroferias.entities.UnidadMedida;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class UnidadMedidaController {
	@Autowired
	private UnidadMedidaService unidadMedidaService;
	
	@RequestMapping(value="/api/producto/unidadesmedida")
	public List<UnidadMedida> unidades(){
		List<UnidadMedida> lista = unidadMedidaService.getActiveUnidadMedida();
		return lista;
	}
	
}