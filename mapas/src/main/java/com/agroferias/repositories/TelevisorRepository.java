package com.agroferias.repositories;

import com.agroferias.entities.Televisor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TelevisorRepository extends JpaRepository<Televisor,Integer> {
    List<Televisor> findAllByIdFeria(Integer idFeria);
    Televisor findByIdTelevisor(Integer idTelevisor);
    Televisor findByIdTelevisorAndIdFeria(Integer idTelevisor , Integer idFeria);
}
