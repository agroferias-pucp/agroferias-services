package com.agroferias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

import com.agroferias.entities.Categoria;
import com.agroferias.services.CategoriaService;
import com.agroferias.services.SubcategoriaService;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class CategoriaController {

    @Autowired
    private CategoriaService categoriaService;

    @Autowired
    private SubcategoriaService subcategoriaService;

    @RequestMapping(value = "/api/producto/categorias")
    public List<Categoria> categorias() {
        List<Categoria> lista = categoriaService.getActiveCategoria();
        return lista;
    }

    @RequestMapping(value = "/api/producto/categoria/{idCategoria}")
    public Categoria categoria(@PathVariable(value = "idCategoria") Integer idCategoria) {
        Categoria lista = categoriaService.getCategoria(idCategoria);
        return lista;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/producto/categorias/registrar", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Categoria registrarCategoria(@RequestBody Categoria categoria) throws Exception {
        categoria.setActivo(1);
        return categoriaService.registarCategoria(categoria);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/producto/categorias/modificar", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    Categoria modificarCategoria(@RequestBody Categoria categoria) throws Exception {
        return categoriaService.actualizarCategoria(categoria);

    }

    @RequestMapping(method = RequestMethod.POST, value = "/api/producto/categorias/eliminar")
    public @ResponseBody
    Integer eliminarCategoria(@RequestParam int idCategoria) {
        Categoria c;

        c = categoriaService.findById(idCategoria);

        if (c == null) {
            return 0;
        }
        if (subcategoriaService.getSubcategoriaCategoria(c.getIdCategoria()) == null || subcategoriaService.getSubcategoriaCategoria(c.getIdCategoria()).isEmpty()) {
            c.setActivo(0);
            categoriaService.actualizarCategoria(c);
            return 1;
        }
        return 0;
    }
}
