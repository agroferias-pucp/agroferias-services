package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from archivos!";
    }

    @RequestMapping(value = "/api/archivos")
    String archivos_api() {
        return "hola api archivos!";
    }

    @RequestMapping(value = "/api/archivos/a/b")
    String archivos_api_a_b() {
        return "hola api archivos a b!";
    }
}