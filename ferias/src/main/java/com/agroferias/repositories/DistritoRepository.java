package com.agroferias.repositories;

import com.agroferias.entities.Distrito;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DistritoRepository extends JpaRepository<Distrito,Integer> {
}
