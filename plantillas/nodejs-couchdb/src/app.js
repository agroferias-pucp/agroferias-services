const express = require('express')
const log = require('debug')('users_auth_service')

const app = express.Router()

app.get('/', (req, res) => {
  res.status(200).json({
    servicio: "ok"
  })
})

app.get('/1', (req, res) => {
  res.status(200).json({
    id:1,
    servicio: "producto id 1"
  })
})

app.get('/1/:anything/:any2', (req, res) => {
  res.status(200).json({
    hateoas: "test"
  })
})
module.exports = app
