package com.agroferias.repositories;

import java.util.List;

import com.agroferias.entities.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Integer>{
    @Query(value="SELECT * FROM usuariosdb.Usuario",nativeQuery = true)
    List<Usuario> findAllUsuarios();
}