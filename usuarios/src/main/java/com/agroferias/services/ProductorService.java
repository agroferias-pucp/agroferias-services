package com.agroferias.services;

import java.util.ArrayList;
import java.util.List;

import com.agroferias.dto.ProductorDTO;
import com.agroferias.entities.Persona;
import com.agroferias.entities.Productor;
import com.agroferias.entities.Usuario;
import com.agroferias.repositories.PersonaRepository;
import com.agroferias.repositories.ProductorRepository;
import com.agroferias.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductorService{
    @Autowired
    private ProductorRepository productorRepo;
    @Autowired
    private PersonaRepository personaRepo;
    @Autowired
    private UsuarioRepository usuarioRepo;

    public List<ProductorDTO> getProductores(){
        List<Productor> listaProds = this.productorRepo.findAll();
        List<ProductorDTO> listaFinal = new ArrayList<ProductorDTO>();
        for (Productor prod : listaProds) {
            Persona per = this.personaRepo.findOne(prod.getIdPersona());
            Usuario u = this.usuarioRepo.findOne(prod.getIdUsuario());
            ProductorDTO p = new ProductorDTO(prod,per,u);
            listaFinal.add(p);
        }
        return listaFinal;
    }
    public Integer eliminarProductor(Integer id){
        productorRepo.delete(id);
        return id;
    }

    public ProductorDTO registrarProductor(ProductorDTO productor){
        Productor prod = new Productor();
        //Registrar a la persona
        Persona p = productor.sacarPersona();
        //Al guardar, se agrega el campo idPersona y se le asigna un valor
        p = personaRepo.save(p);
        //Registrar al usuario
        Usuario u = productor.sacarUsuario();
        //Al guardar, se agrega el campo idUsuario y se le asigna un valor
        u = usuarioRepo.save(u);
        //Ahora con el idPersona e idUsuario, registrarlo
        prod.setDni(productor.getDni());
        prod.setIdPersona(p.getIdPersona());
        prod.setIdUsuario(u.getIdUsuario());
        prod.setIdEmpresa(productor.getIdEmpresa());
        prod.setIdCategoria(productor.getIdCategoria());
        prod.setIdResponsable(productor.getIdResponsable());
        prod.setObservaciones(productor.getObservaciones());
        prod.setIdProductoPrincipal(productor.getIdProductoPrincipal());
        prod = productorRepo.save(prod);
        //con el save se tiene el id
        productor= new ProductorDTO(prod, p, u);
        return productor;
    }
    public ProductorDTO getProductorDTO(Integer id){
        Productor prod = this.productorRepo.findOne(id);
        Persona per = this.personaRepo.findOne(prod.getIdPersona());
        Usuario u = this.usuarioRepo.findOne(prod.getIdUsuario());
        ProductorDTO p = new ProductorDTO(prod,per,u);
        return p;
    }
}