package com.agroferias.services;

import java.util.List;
import com.agroferias.entities.Rol;
import com.agroferias.repositories.RolRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolService{
    @Autowired
    private RolRepository rolRepo;
    public List<Rol> getRoles(){
        return rolRepo.findAllRoles();
    }
}