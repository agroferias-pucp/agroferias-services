package com.agroferias.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.agroferias.entities.Empresa;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {

    @Query(value = "SELECT * FROM Empresa a WHERE a.activo = 1", nativeQuery = true)
    List<Empresa> findAllActiveEmpresa();

    public Empresa findByIdEmpresa(Integer idEmpres);
}
