package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from cupones!";
    }

    @RequestMapping(value = "/api/cupones")
    String cupones_api() {
        return "hola api cupones!";
    }

    @RequestMapping(value = "/api/cupones/a/b")
    String cupones_api_a_b() {
        return "hola api cupones a b!";
    }
}