package com.agroferias.services;

import java.util.List;

import com.agroferias.dto.GestorDTO;
import com.agroferias.entities.Gestor;
import com.agroferias.entities.Persona;
import com.agroferias.entities.Usuario;
import com.agroferias.repositories.GestorRepository;
import com.agroferias.repositories.PersonaRepository;
import com.agroferias.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GestorService{
    @Autowired
    private GestorRepository gestorRepo;
    @Autowired
    private PersonaRepository personaRepo;
    @Autowired
    private UsuarioRepository usuarioRepo;

    public List<Gestor> getGestores(){
        return gestorRepo.findAll();
    }

    public Integer eliminarGestor(Integer id){
        gestorRepo.delete(id);
        return id;
    }
    
    public GestorDTO registrarGestor(GestorDTO gestor){
        Gestor g = new Gestor();
        //Registrar a la persona
        Persona p = new Persona(gestor.getNombres(), gestor.getApellidoPaterno(), 
                    gestor.getApellidoMaterno(),gestor.getTelefono(),gestor.getCorreo());

        //Al guardar, se agrega el campo idPersona y se le asigna un valor
        p = personaRepo.save(p);
        //Registrar al usuario
        Usuario u = new Usuario(gestor.getIdRol(), gestor.getUsername(), gestor.getPassword(), gestor.getFechaRegistro());
        u = usuarioRepo.save(u);
        //Ahora con el idPersona e idUsuario, registrarlo
        g.setDni(gestor.getDni());
        g.setIdPersona(p.getIdPersona());
        g.setIdUsuario(u.getIdUsuario());
        g= gestorRepo.save(g);

        //con el save se tiene el id
        gestor.setIdGestor(g.getIdGestor());
        return gestor;
    }
    public GestorDTO getGestorDTO(Integer id){
        Gestor gest = this.gestorRepo.findOne(id);
        Persona per = this.personaRepo.findOne(gest.getIdPersona());
        Usuario u = this.usuarioRepo.findOne(gest.getIdUsuario());
        GestorDTO g = new GestorDTO(gest,per,u);
        return g;
    }
}