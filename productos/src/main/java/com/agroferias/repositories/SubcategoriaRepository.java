package com.agroferias.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.agroferias.entities.Subcategoria;

@Repository
public interface SubcategoriaRepository extends JpaRepository<Subcategoria, Integer> {
	@Query(value = "SELECT * FROM Subcategoria a WHERE a.activo = 1", nativeQuery = true)
	List<Subcategoria> findAllActiveSubcategoria();

	@Query(value = "SELECT * FROM Subcategoria a WHERE a.activo = 1 and a.idCategoria = ?1", nativeQuery = true)
	List<Subcategoria> findAllActiveSubcategoriaCategoria(Integer idCategoria);

	@Query(value = "SELECT * FROM Subcategoria a WHERE a.idSubcategoria = ?1", nativeQuery = true)
	Optional<Subcategoria> findByIdSubcategoria(Integer idSubcategoria);
}