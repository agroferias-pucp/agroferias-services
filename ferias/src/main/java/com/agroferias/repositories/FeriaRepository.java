package com.agroferias.repositories;

import com.agroferias.entities.Feria;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FeriaRepository extends JpaRepository<Feria,Integer> {
    Feria findByIdFeria(Integer idFeria);
}
