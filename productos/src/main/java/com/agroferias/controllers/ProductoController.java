package com.agroferias.controllers;

import com.agroferias.entities.Producto;
import com.agroferias.services.ProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @RequestMapping(method = RequestMethod.GET, value = "/api/productos/tienda/{idTienda}")
    public List<Producto> listarProductosByTienda(@PathVariable(value = "idTienda") int idTienda) {
        return productoService.findProductosByTienda(idTienda);
    }

    /*@RequestMapping(method = RequestMethod.POST, value = "/api/producto/registrar", produces = {MediaType.APPLICATION_JSON_VALUE})
    public Producto agregarProducto(@RequestBody Producto producto) throws Exception{
        //return producto;
        return productoService.guardarProducto(producto);
    }*/
    
    @RequestMapping(value = "/api/producto/modificar", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Producto editar(@RequestBody Producto nuevo) throws Exception {
        return productoService.editarProducto(nuevo);
    }

    @RequestMapping(value = "/api/producto/eliminar", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Integer eliminarProducto(@RequestParam(value = "idProducto") int idProducto) throws Exception {
        return productoService.eliminarProducto(idProducto);
    }

}
