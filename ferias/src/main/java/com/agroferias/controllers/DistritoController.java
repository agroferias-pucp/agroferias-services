package com.agroferias.controllers;

import com.agroferias.entities.Distrito;
import com.agroferias.repositories.DistritoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/feria/distrito")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})

public class DistritoController {

    @Autowired
    DistritoRepository distritoRepository;

    @RequestMapping(value = "/listarDistritos", method = RequestMethod.GET)
    public List<Distrito> listarDistritos(){
        return distritoRepository.findAll();
    }
}
