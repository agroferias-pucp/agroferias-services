package com.agroferias.controllers;

import com.agroferias.dto.AutenticationDTO;
import com.agroferias.dto.ClienteDTO;
import com.agroferias.services.ClienteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController()
@RequestMapping(value = "/api/usuario/cliente")
@CrossOrigin(origins="*",methods={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class ClienteController{
    @Autowired
    private ClienteService servicioClientes = new ClienteService();

    @RequestMapping(value="/registro",method=RequestMethod.POST)
    public ClienteDTO registrarCliente(@RequestBody ClienteDTO clienteRecibido){
        clienteRecibido = this.servicioClientes.registrarcliente(clienteRecibido);
        return clienteRecibido;
    }
    @RequestMapping(value="/autenticacion",method=RequestMethod.POST)
    public Integer validarCliente(@RequestBody AutenticationDTO credenciales){
        //validar usuario
        return 1;
    }  
    @RequestMapping(value="/{idCliente}")
    public ClienteDTO getClienteDTO(@PathVariable("idCliente") int id){
        return this.servicioClientes.getClienteDTO(id);
    }

    @RequestMapping(value="/eliminar/{idCliente}")
    public Integer eliminarCliente(@PathVariable("idCliente") int id){
        this.servicioClientes.eliminarCliente(id);
        return id;
    }
}