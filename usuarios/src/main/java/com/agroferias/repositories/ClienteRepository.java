package com.agroferias.repositories;

import java.util.List;

import com.agroferias.entities.Cliente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Integer>{
    @Query(value="SELECT * FROM usuariosdb.Cliente",nativeQuery = true)
    List<Cliente> findAllClientes();
}