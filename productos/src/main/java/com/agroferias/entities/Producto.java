package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Lob;

@Entity
@Table(name = "Producto")
public class Producto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idProducto")
    private Integer idProducto;

    @ManyToOne
    @JoinColumn(name = "idSubCategoria")
    private Subcategoria subCategoria;

    @ManyToOne
    @JoinColumn(name = "idUnidadMedida")
    private UnidadMedida unidadMedida;

    @ManyToOne
    @JoinColumn(name = "idSolicitudProducto")
    private SolicitudProducto solicitudProducto;

    @Column(name = "idTienda")
    private int idTienda;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "codProducto")
    private String codProducto;

    @Column(name = "precio")
    private Double precio;
    
    @Column(name = "stock")
    private Double stock;
    
    /*@Lob
    @Column(name = "imagen")
    private Blob imagen;*/

    
    @Lob
    @Column(name = "descripcion", length=1024)
    private String descripcion;
    
    @Lob
    @Column(name = "valorNutricional", length=1024)
    private String valorNutricional;
    
    @Column(name = "activo")
    private int activo;



    public Producto() {
    }

    public Producto(Integer idProducto, Subcategoria subCategoria, UnidadMedida unidadMedida, SolicitudProducto solicitudProducto, int idTienda, String nombre, String codProducto, Double precio, Double stock, String descripcion, String valorNutricional, int activo) {
        this.idProducto = idProducto;
        this.subCategoria = subCategoria;
        this.unidadMedida = unidadMedida;
        this.solicitudProducto = solicitudProducto;
        this.idTienda = idTienda;
        this.nombre = nombre;
        this.codProducto = codProducto;
        this.precio = precio;
        this.stock = stock;
        this.descripcion = descripcion;
        this.valorNutricional = valorNutricional;
        this.activo = activo;
    }

    public Integer getIdProducto() {
        return this.idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Subcategoria getSubCategoria() {
        return this.subCategoria;
    }

    public void setSubCategoria(Subcategoria subCategoria) {
        this.subCategoria = subCategoria;
    }

    public UnidadMedida getUnidadMedida() {
        return this.unidadMedida;
    }

    public void setUnidadMedida(UnidadMedida unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public SolicitudProducto getSolicitudProducto() {
        return this.solicitudProducto;
    }

    public void setSolicitudProducto(SolicitudProducto solicitudProducto) {
        this.solicitudProducto = solicitudProducto;
    }

    public int getIdTienda() {
        return this.idTienda;
    }

    public void setIdTienda(int idTienda) {
        this.idTienda = idTienda;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodProducto() {
        return this.codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public Double getPrecio() {
        return this.precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Double getStock() {
        return this.stock;
    }

    public void setStock(Double stock) {
        this.stock = stock;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValorNutricional() {
        return this.valorNutricional;
    }

    public void setValorNutricional(String valorNutricional) {
        this.valorNutricional = valorNutricional;
    }

    public int getActivo() {
        return this.activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }
    

}
