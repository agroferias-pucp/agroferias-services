package com.agroferias.services;

import com.agroferias.dto.ClienteDTO;
import com.agroferias.entities.Cliente;
import com.agroferias.entities.Persona;
import com.agroferias.entities.Usuario;
import com.agroferias.repositories.ClienteRepository;
import com.agroferias.repositories.PersonaRepository;
import com.agroferias.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService{
    @Autowired
    private ClienteRepository clienteRepo;
    @Autowired
    private PersonaRepository personaRepo;
    @Autowired
    private UsuarioRepository usuarioRepo;

    public Integer eliminarCliente(Integer id){
        clienteRepo.delete(id);
        return id;
    }

    public ClienteDTO registrarcliente(ClienteDTO cliente){
        Cliente c = new Cliente();
        //Registrar a la persona
        Persona p = new Persona(cliente.getNombres(), cliente.getApellidoPaterno(), 
                    cliente.getApellidoMaterno(),cliente.getTelefono(),cliente.getCorreo());

        //Al guardar, se agrega el campo idPersona y se le asigna un valor
        p = personaRepo.save(p);
        //Registrar al usuario
        Usuario u = new Usuario(cliente.getIdRol(), cliente.getUsername(), cliente.getPassword(), cliente.getFechaRegistro());
        u = usuarioRepo.save(u);
        //Ahora con el idPersona e idUsuario, registrarlo
        c.setIdPersona(p.getIdPersona());
        c.setIdUsuario(u.getIdUsuario());
        c.setDni(cliente.getDni());
        c = clienteRepo.save(c);
        //con el save se tiene el id
        cliente.setIdCliente(c.getIdCliente());
        return cliente;
    }
    public ClienteDTO getClienteDTO(Integer id){
        Cliente cli = this.clienteRepo.findOne(id);
        Persona per = this.personaRepo.findOne(cli.getIdPersona());
        Usuario u = this.usuarioRepo.findOne(cli.getIdUsuario());
        ClienteDTO c = new ClienteDTO(cli,per,u);
        return c;
    }
}