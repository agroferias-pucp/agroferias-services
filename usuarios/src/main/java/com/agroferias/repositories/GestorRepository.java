package com.agroferias.repositories;

import com.agroferias.entities.Gestor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GestorRepository extends JpaRepository<Gestor,Integer>{
}