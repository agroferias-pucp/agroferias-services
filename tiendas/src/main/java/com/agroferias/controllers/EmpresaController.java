package com.agroferias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.agroferias.entities.Empresa;
import com.agroferias.services.EmpresaService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class EmpresaController {

    @Autowired
    private EmpresaService empresaService;

    @RequestMapping(value = "/api/empresas")
    public List<Empresa> empresas() {
        List<Empresa> lista = empresaService.getActiveEmpresas();
        return lista;
    }

    @RequestMapping(value = "/api/empresas/all")
    public List<Empresa> allEmpresas() {
        return empresaService.allEmpresas();
    }

    @RequestMapping(value = "/api/empresa/perfil/{idEmpresa}", method = RequestMethod.GET)
    public Empresa perfil(@PathVariable(value = "idEmpresa") int idEmpresa) {
        Empresa empresa = empresaService.servicioPerfilEmpresa(idEmpresa);
        if (empresa == null) {
            return null;
        }
        return empresa;
    }

    @RequestMapping(value = "/api/empresa/registrar", method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    Empresa crear(@RequestBody Empresa empresa) {
        empresaService.registrarEmpresa(empresa);
        return empresa;
    }

    @RequestMapping(value = "/api/empresa/eliminar", method = RequestMethod.POST)
    public Integer eliminarEmpresa(@RequestParam int idEmpresa) throws Exception {
        return empresaService.eliminarEmpresa(idEmpresa);
    }

    @RequestMapping(value = "/api/empresa/modificar", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Empresa editar(@RequestBody Empresa nuevo) throws Exception {
        return empresaService.editarEmpresa(nuevo);
    }

}
