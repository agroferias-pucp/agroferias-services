package com.agroferias.repositories;

import java.util.List;

import com.agroferias.entities.Productor;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductorRepository extends JpaRepository<Productor,Integer>{
    @Query(value="SELECT * FROM usuariosdb.Productor",nativeQuery = true)
    List<Productor> findAllProductores();
}