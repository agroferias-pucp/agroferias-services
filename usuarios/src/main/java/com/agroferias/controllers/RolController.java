package com.agroferias.controllers;

import java.util.List;

import com.agroferias.entities.Rol;
import com.agroferias.services.RolService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
@CrossOrigin(origins="*",methods={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class RolController{
    @Autowired
    private RolService servicioRoles = new RolService();

    @RequestMapping(value="/api/usuario/roles")
    public List<Rol> listarRoles(){
        List<Rol> lista = servicioRoles.getRoles();
        return lista;
    }
    //codigo
}