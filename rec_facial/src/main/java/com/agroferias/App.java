package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from rec_facial!";
    }

    @RequestMapping(value = "/api/rec_facial")
    String rec_facial_api() {
        return "hola api rec_facial!";
    }

    @RequestMapping(value = "/api/rec_facial/a/b")
    String rec_facial_api_a_b() {
        return "hola api rec_facial a b!";
    }
}