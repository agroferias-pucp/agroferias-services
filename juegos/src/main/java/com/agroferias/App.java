package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from juegos!";
    }

    @RequestMapping(value = "/api/juegos")
    String juegos_api() {
        return "hola api juegos!";
    }

    @RequestMapping(value = "/api/juegos/a/b")
    String juegos_api_a_b() {
        return "hola api juegos a b!";
    }
}