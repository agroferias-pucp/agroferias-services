package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from publicidad!";
    }

    @RequestMapping(value = "/api/publicidad")
    String publicidad_api() {
        return "hola api publicidad!";
    }

    @RequestMapping(value = "/api/publicidad/a/b")
    String publicidad_api_a_b() {
        return "hola api publicidad a b!";
    }
}