package com.agroferias.services;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import com.agroferias.entities.Categoria;
import com.agroferias.repositories.CategoriaRepository;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    public List<Categoria> getActiveCategoria() {
        return categoriaRepository.findAllActiveCategoria();
    }

    public Categoria getCategoria(Integer idCategoria) {
        return categoriaRepository.findByIdCategoria(idCategoria);
    }

    public Categoria registarCategoria(Categoria c) {
        return categoriaRepository.save(c);
    }

    public Categoria findById(int idCategoria) {
        return categoriaRepository.findOne(idCategoria);
    }

    public Categoria actualizarCategoria(Categoria c) {
        Categoria categoria = categoriaRepository.findByIdCategoria(c.getIdCategoria());
        if (categoria != null) {
            categoria.setDescripcion(c.getDescripcion());
            categoria.setNombre(c.getNombre());
            return categoriaRepository.save(categoria);
        }
        return null;
    }
}
