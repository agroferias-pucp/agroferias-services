package com.agroferias.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.agroferias.entities.Tienda;

@Repository
public interface TiendaRepository extends JpaRepository<Tienda, Integer> {

    @Query(value = "SELECT * FROM Tienda a WHERE a.activo = 1 AND a.idEmpresa = ?1", nativeQuery = true)
    List<Tienda> findAllTiendaEmpresa(Integer idEmpresa);

    @Query(value = "SELECT * FROM Tienda a WHERE a.activo = 1 AND a.idFeria = ?1", nativeQuery = true)
    List<Tienda> findAllTiendaFeria(Integer idFeria);

    @Query(value = "SELECT * FROM Tienda a WHERE a.activo = 1 AND a.idFeria = ?1 AND a.tipoTienda = ?2", nativeQuery = true)
    List<Tienda> findAllTiendaFeriaTipo(Integer idFeria, Integer tipoTienda);

    public Tienda findByIdTienda(Integer idTienda);
}
