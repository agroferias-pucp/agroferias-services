package com.agroferias.services;

import java.util.List;


import com.agroferias.entities.Usuario;
import com.agroferias.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService{
    @Autowired
    private UsuarioRepository usuariosRepo;
    public List<Usuario> getUsuarios(){
        return usuariosRepo.findAllUsuarios();
    }
}