package com.agroferias.repositories;

import com.agroferias.entities.SolicitudProducto;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SolicitudProductoRepository extends JpaRepository<SolicitudProducto, Integer>{
    
    @Query(value = "SELECT * FROM SolicitudProducto a WHERE a.activo = 1 AND a.idFeria = ?1", nativeQuery = true)
    List<SolicitudProducto> findByIdFeria(Integer idFeria);

    @Query(value = "SELECT * FROM SolicitudProducto a WHERE a.activo = 1 AND a.idTienda = ?1", nativeQuery = true)
    List<SolicitudProducto> findByIdTienda(Integer idTienda);

    @Query(value = "SELECT * FROM SolicitudProducto a WHERE a.activo = 1 AND a.idSolicitud = ?1", nativeQuery = true)
    Optional<SolicitudProducto> findByIdSolicitud(Integer idSolicitud);
    
    
}
