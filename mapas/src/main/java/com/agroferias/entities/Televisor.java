package com.agroferias.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Televisor")
public class Televisor implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idTelevisor;

    @Column(name = "idFeria")
    private Integer idFeria;

    @Column(name = "posicion_X")
    private Double posicion_X;

    @Column(name = "posicion_Y")
    private Double posicion_Y;

    @Column(name = "habilitado")
    private Integer habilitado; // habilitado 1 , no habilitado 0

    public Televisor() {
    }

    @PrePersist
    public  void setDefaultValues(){
        habilitado = 1;
    }

    public Televisor(Integer idFeria, Double posicion_X, Double posicion_Y, Integer habilitado) {
        this.idFeria = idFeria;
        this.posicion_X = posicion_X;
        this.posicion_Y = posicion_Y;
        this.habilitado = habilitado;
    }

    public Televisor(Integer idTelevisor,Integer idFeria, Double posicion_X, Double posicion_Y, Integer habilitado) {
        this.idTelevisor = idTelevisor;
        this.idFeria = idFeria;
        this.posicion_X = posicion_X;
        this.posicion_Y = posicion_Y;
        this.habilitado = habilitado;
    }


    public Integer getIdTelevisor() {
        return idTelevisor;
    }

    public Integer getIdFeria() {
        return idFeria;
    }

    public void setIdFeria(Integer idFeria) {
        this.idFeria = idFeria;
    }

    public Double getPosicion_X() {
        return posicion_X;
    }

    public void setPosicion_X(Double posicion_X) {
        this.posicion_X = posicion_X;
    }

    public Double getPosicion_Y() {
        return posicion_Y;
    }

    public void setPosicion_Y(Double posicion_Y) {
        this.posicion_Y = posicion_Y;
    }

    public Integer getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Integer habilitado) {
        this.habilitado = habilitado;
    }
}
