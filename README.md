# DP2 Arquitectura

```bash
docker swarm join --token SWMTKN-1-0qqxigh5h2g8764q4ogo604q1r34bf1u5pwckp7a274hk7wx9f-3g5lzdtvimev0o5pri9lxyjh1 10.0.22.152:2377
```

## Agregar un nuevo servicio

* Copiar platilla de *test-java-maven*

* Cambiar al nombre de la carpeta por el nombre del <servicio>

* En el archivo *docker-compose.yml*
  * Copiar configuración de servicio *test-java-maven* (dentro de services) 
  * Actualizar nombre en *image* por <servicio>
  * Actualizar  puerto de <servicio> (seguir documento **asignacion** backend)
* En el archivo *.gitlab-ci.yml*
  * Copiar plantilla de pedidos del job *build_preprod*
  * Actualizar pedidos por <servicio>
* Para finalizar, comprobar con el navegador web el IP 18.189.132.139:<puerto>
  (tambien comprobable con gitlab)

## Flujo de rollback

1. Entrar al VM dev1 con login.sh

2. Ejecutar comando
   	

   ```bash
   docker stack rm agroferias && sleep 3 && printf "y\n" | docker system prune && sleep 3
   ```
   
3. Ir a la página web del gitlab y ejecutar de nuevo el último proceso de pipeline (las dos etapas)

## Servicios

| servicio        | nodo | puerto     |
| --------------- | ---- | ---------- |
| pedidos         | dev4 | 3001 - On  |
| reclamos        | dev4 | 3002 - On  |
| promociones     | dev4 | 3003 - On  |
| tiendas_fav     | dev4 | 3004 - On  |
| buscador_solr   | -    | 3005 - Off |
| recomendaciones | -    | 3006 - Off |
| movimientos     | dev2 | 3101 - On  |
| tiendas         | dev2 | 3102 - On  |
| productos       | dev2 | 3103 - On  |
| ferias          | dev5 | 3201 - On  |
| pagos           | dev5 | 3203 - On  |
| archivos        | dev5 | 3204 - On  |
| rec_facial      | dev3 | 3301 - On  |
| publicidad      | dev3 | 3302 - On  |
| juegos          | dev3 | 3303 - On  |
| mapas           | dev1 | 3304 - On  |
| cupones         | dev1 | 3305 - On  |