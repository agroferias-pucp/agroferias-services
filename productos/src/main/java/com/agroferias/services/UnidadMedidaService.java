package com.agroferias.services;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import com.agroferias.entities.UnidadMedida;
import com.agroferias.repositories.UnidadMedidaRepository;

@Service
public class UnidadMedidaService {
	
	@Autowired 
	private UnidadMedidaRepository unidadMedidaRepository;
	
	public List<UnidadMedida> getActiveUnidadMedida(){
		return unidadMedidaRepository.findAllActiveUnidadMedida();
	}	
	
}