package com.agroferias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.MediaType;

import com.agroferias.entities.Categoria;
import com.agroferias.entities.Subcategoria;
import com.agroferias.services.SubcategoriaService;
import org.springframework.web.bind.annotation.CrossOrigin;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class SubcategoriaController {
	@Autowired
	private SubcategoriaService subcategoriaService;
	
	@RequestMapping(value="/api/producto/subcategorias")
	public List<Subcategoria> subcategorias(){
		List<Subcategoria> lista = subcategoriaService.getActiveSubcategoria();		
		return lista;
	}
	
	@RequestMapping(value="/api/producto/subcategorias/categoria/{idCategoria}", method = RequestMethod.GET)
	public List<Subcategoria> subcategoriasCategoria(@PathVariable(value = "idCategoria") Integer idCategoria){
		List<Subcategoria> lista = subcategoriaService.getSubcategoriaCategoria(idCategoria);
		return lista;
	}

	@RequestMapping(value="/api/producto/subcategoria/registrar", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
	public @ResponseBody Subcategoria registrarSubcategoria (@RequestBody Subcategoria subcategoria) throws Exception {
		return subcategoriaService.insert(subcategoria);
	}

	@RequestMapping(value="/api/producto/subcategoria/modificar", method = RequestMethod.POST)
	public @ResponseBody Subcategoria modificarSubcategoria (@RequestBody Subcategoria subcategoria) throws Exception {
		return subcategoriaService.update(subcategoria);
	}

	@RequestMapping(value="/api/producto/subcategoria/eliminar", method = RequestMethod.POST)
	public @ResponseBody Integer eliminarSubcategoria (@RequestParam Integer idSubcategoria){
		Subcategoria subcategoria = new Subcategoria();
		subcategoria.setIdSubcategoria(idSubcategoria);
		subcategoriaService.delete(subcategoria);
		return idSubcategoria;
	}
}