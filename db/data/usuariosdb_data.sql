
use usuariosdb;

INSERT INTO Rol(idRol,descripcion) VALUES (1,'Administrador');
INSERT INTO Rol(idRol,descripcion) VALUES (2,'Gestor Acopio');
INSERT INTO Rol(idRol,descripcion) VALUES (3,'Gestor Agroferia');
INSERT INTO Rol(idRol,descripcion) VALUES (4,'Cliente');
INSERT INTO Rol(idRol,descripcion) VALUES (5,'Productor');
INSERT INTO Rol(idRol,descripcion) VALUES (6,'Productor Auxiliar');


INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (1,1,'napsta32','napsta','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (2,2,'holis','holis','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (3,3,'froz','pass','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (4,4,'andresg','pass','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (5,5,'shaggs','adsobd','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (6,2,'srivas','quemalos','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (7,4,'luisarana','friends','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (8,6,'cvargas','vargas','07/10/2019');
INSERT INTO Usuario(idUsuario,idRol,username,password,fechaRegistro) VALUES (9,6,'itu','itu','07/10/2019');


INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (1,'Franco','Peña','Campos','951452368','fpenac@pucp.edu.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (2,'Eduardo','Velarde','Polar','958964123','eduardo.velarde@pucp.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (3,'Arian','Gallardo','Callalli','987654321','gallardo.a@pucp.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (4,'Andres','Gonzales','Delgado','999548159','andres.gonzales@pucp.edu.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (5,'Sebastian','Ganto','Huarhua','984174189','sebastian.ganto@pucp.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (6,'Sergio','Rivas','Medina','998562500','sergio.rivas@pucp.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (7,'Luis','Arana','Motta','989561045','luis.arana@pucp.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (8,'Carlos','Vargas','Rioja','987569874','cvargasr@pucp.pe');
INSERT INTO Persona(idPersona,nombres,apellidoPaterno,apellidoMaterno,telefono,correo) VALUES (9,'Rodrigo','Horruitiner','Mendoza','984562998','rhorruitiner@pucp.edu.pe');


INSERT INTO Cliente(idPersona,idCliente,idUsuario,DNI) VALUES (4,1,4,'74895410');
INSERT INTO Cliente(idPersona,idCliente,idUsuario,DNI) VALUES (6,2,6,'74139856');
INSERT INTO Cliente(idPersona,idCliente,idUsuario,DNI) VALUES (7,3,7,'73414516');


INSERT INTO Gestor(idPersona,idGestor,DNI,idUsuario) VALUES (1,1,'78945816',1);
INSERT INTO Gestor(idPersona,idGestor,DNI,idUsuario) VALUES (2,2,'78451210',2);
INSERT INTO Gestor(idPersona,idGestor,DNI,idUsuario) VALUES (3,3,'78495623',3);



