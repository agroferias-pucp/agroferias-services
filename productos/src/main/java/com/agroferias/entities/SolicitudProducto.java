package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Lob;
import java.sql.Blob;
import java.sql.Timestamp;

@Entity
@Table(name = "SolicitudProducto")
public class SolicitudProducto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSolicitudProducto")
    private Integer idSolicitudProducto;

    @ManyToOne
    @JoinColumn(name = "idSubCategoria")
    private Subcategoria subCategoria;

    @ManyToOne
    @JoinColumn(name = "idUnidadMedida")
    private UnidadMedida unidadMedida;

    @Column(name = "idSolicitudTienda")
    private Integer idSolicitudTienda;

    @Column(name = "idSolicitudEmpresa")
    private Integer idSolicitudEmpresa;

    @Column(name = "idTienda")
    private Integer idTienda;
    
    @Column(name = "nombre")
    private String nombre;

    @Column(name = "precio")
    private Double precio;

    @Lob
    @Column(name = "imagen", columnDefinition="blob")
    private Blob imagen;

    @Lob
    @Column(name = "descripcion", length=1024)
    private String descripcion;

    @Lob
    @Column(name = "valorNutricional", length=1024)
    private String valorNutricional;

    @Lob
    @Column(name = "razon", length=1024)
    private String razon;
    
    @Column(name = "idUsuarioRegistro")
    private Integer idUsuarioRegistro;

    @Column(name = "idUsuarioRespuesta")
    private Integer idUsuarioRespuesta;

    @Column(name = "estado")
    private Integer estado;

    @Column(name = "fechaRegistro")
    private Timestamp fechaRegistro;

    @Column(name = "fechaRespuesta")
    private Timestamp fechaRespuesta;

    @Column(name = "activo")
    private Integer activo;



    public SolicitudProducto() {
    }

    public SolicitudProducto(Integer idSolicitudProducto, Subcategoria subCategoria, UnidadMedida unidadMedida, 
            Integer idSolicitudTienda, Integer idSolicitudEmpresa, Integer idTienda, String nombre, Double precio, 
            String descripcion, String valorNutricional, String razon, Integer idUsuarioRegistro, 
            Integer idUsuarioRespuesta, Integer estado, Timestamp fechaRegistro, Timestamp fechaRespuesta, Integer activo) {
        this.idSolicitudProducto = idSolicitudProducto;
        this.subCategoria = subCategoria;
        this.unidadMedida = unidadMedida;
        this.idSolicitudTienda = idSolicitudTienda;
        this.idSolicitudEmpresa = idSolicitudEmpresa;
        this.idTienda = idTienda;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
        this.valorNutricional = valorNutricional;
        this.razon = razon;
        this.idUsuarioRespuesta = idUsuarioRespuesta;
        this.idUsuarioRegistro = idUsuarioRegistro;
        this.estado = estado;
        this.fechaRegistro = fechaRegistro;
        this.fechaRespuesta = fechaRespuesta;
        this.activo = activo;
    }

    public Integer getIdSolicitudProducto() {
        return this.idSolicitudProducto;
    }

    public void setIdSolicitudProducto(Integer idSolicitudProducto) {
        this.idSolicitudProducto = idSolicitudProducto;
    }

    public Subcategoria getSubCategoria() {
        return this.subCategoria;
    }

    public void setSubCategoria(Subcategoria subCategoria) {
        this.subCategoria = subCategoria;
    }

    public UnidadMedida getUnidadMedida() {
        return this.unidadMedida;
    }

    public void setUnidadMedida(UnidadMedida unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public Integer getIdSolicitudTienda() {
        return this.idSolicitudTienda;
    }

    public void setIdSolicitudTienda(Integer idSolicitudTienda) {
        this.idSolicitudTienda = idSolicitudTienda;
    }

    public Integer getIdSolicitudEmpresa() {
        return this.idSolicitudEmpresa;
    }

    public void setIdSolicitudEmpresa(Integer idSolicitudEmpresa) {
        this.idSolicitudEmpresa = idSolicitudEmpresa;
    }

    public Integer getIdTienda() {
        return this.idTienda;
    }

    public void setIdTienda(Integer idTienda) {
        this.idTienda = idTienda;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return this.precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Blob getImagen(){
        return this.imagen;
    }

    public void setImagen(Blob imagen){
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValorNutricional() {
        return this.valorNutricional;
    }

    public void setValorNutricional(String valorNutricional) {
        this.valorNutricional = valorNutricional;
    }

    public String getRazon() {
        return this.razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public Integer getIdUsuarioRegistro(){
        return this.idUsuarioRegistro;
    }

    public void setIdUsuarioRegistro(Integer idUsuarioRegistro){
        this.idUsuarioRegistro = idUsuarioRegistro;
    }

    public Integer getIdUsuarioRespuesta(){
        return this.idUsuarioRespuesta;
    }

    public void setIdUsuarioRespuesta(Integer idUsuarioRespuesta){
        this.idUsuarioRespuesta = idUsuarioRespuesta;
    }

    public Integer getEstado(){
        return this.estado;
    }

    public void setEstado(Integer estado){
        this.estado = estado;
    }

    public Timestamp getFechaRegistro(){
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro){
        this.fechaRegistro = fechaRegistro;
    }

    public Timestamp getFechaRespuesta(){
        return this.fechaRespuesta;
    }

    public void setFechaRespuesta(Timestamp fechaRespuesta){
        this.fechaRespuesta = fechaRespuesta;
    }


    public Integer getActivo() {
        return this.activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

}
