package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Gestor")
public class Gestor{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idGestor")
    private Integer idGestor;

    @Column(name="idPersona")
    private Integer idPersona;
    @Column(name="idUsuario")
    private Integer idUsuario;
    @Column(name="DNI")
    private String dni;

    public Gestor(){

    }
    public Gestor(Integer idPersona, Integer idUsuario, String dni){
        this.idPersona=idPersona;
        this.idUsuario=idUsuario;
        this.dni=dni;
    }

    public Integer getIdGestor(){
        return this.idGestor;
    }
    public Integer getIdPersona(){
        return this.idPersona;
    }
    public Integer getIdUsuario(){
        return this.idUsuario;
    }
    public String getDni(){
        return this.dni;
    }

    public void setIdGestor(Integer idGestor){
        this.idGestor=idGestor;
    }
    public void setIdPersona(Integer idPersona){
        this.idPersona=idPersona;
    }
    public void setIdUsuario(Integer idUsuario){
        this.idUsuario=idUsuario;
    }
    public void setDni(String dni){
        this.dni=dni;
    }
}