package com.agroferias.services;

import com.agroferias.entities.Producto;
import com.agroferias.repositories.ProductoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    public List<Producto> findProductosByTienda(int idTienda) {
        return productoRepository.findAllByIdTienda(idTienda);
    }

    public Producto guardarProducto(Producto producto) {
        return productoRepository.save(producto);
    }

    public Producto editarProducto(Producto nuevo) {
        Producto producto = productoRepository.findByIdProducto(nuevo.getIdProducto());
        if (producto != null) { //si loencuentra lo modifica y retorna el producto 
            producto.setSubCategoria(nuevo.getSubCategoria());
            producto.setUnidadMedida(nuevo.getUnidadMedida());
            producto.setSolicitudProducto(nuevo.getSolicitudProducto());
            producto.setIdTienda(nuevo.getIdTienda());
            producto.setNombre(nuevo.getNombre());
            producto.setCodProducto(nuevo.getCodProducto());
            producto.setPrecio(nuevo.getPrecio());
            producto.setStock(nuevo.getStock());
            producto.setDescripcion(nuevo.getDescripcion());
            producto.setValorNutricional(nuevo.getValorNutricional());
            producto.setActivo(nuevo.getActivo());

            return productoRepository.save(producto);
        }
        return null;
    }

    public int eliminarProducto(int idProducto) {
        Producto producto = productoRepository.findByIdProducto(idProducto);
        if (producto != null) {
            producto.setActivo(0);
            productoRepository.save(producto);
            return idProducto;
        } else {
            return 0;
        }
    }
}
