#!/bin/bash
# any environment variable in this script is defined, this should
# be done during the deployment of a service or in a Dockerfile

set -o nounset # Treat unset variables as an error

until curl -X PUT admin:admin@$STORAGE_IP:9002/users ; do
  echo "Users DB wasn't created - sleeping"
  sleep 1
done
echo "DB of users was created!"

