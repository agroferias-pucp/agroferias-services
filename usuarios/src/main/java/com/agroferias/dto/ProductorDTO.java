package com.agroferias.dto;

import java.util.Date;

import com.agroferias.entities.Persona;
import com.agroferias.entities.Productor;
import com.agroferias.entities.Usuario;

public class ProductorDTO {
    private Integer idProductor;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private String correo;
    private Integer idRol;
    private String username;
    private String password;
    private Date fechaRegistro;
    private String dni;
    private Integer idEmpresa;
    private Integer idCategoria;
    private Integer idResponsable;
    private String observaciones;
    private Integer idProductoPrincipal;

    public ProductorDTO() {

    }
    public ProductorDTO(Productor prod,Persona per, Usuario u){
        this.setIdProductor(prod.getIdProductor());
        this.setNombres(per.getNombres());
        this.setApellidoPaterno(per.getApellidoPaterno());
        this.setApellidoMaterno(per.getApellidoMaterno());
        this.setTelefono(per.getTelefono());
        this.setCorreo(per.getCorreo());
        this.setIdRol(u.getIdRol());
        this.setUsername(u.getUsername());
        this.setPassword(u.getPassword());
        this.setFechaRegistro(u.getFechaRegistro());
        this.setDni(prod.getDni());
        this.setIdEmpresa(prod.getIdEmpresa());
        this.setIdCategoria(prod.getIdCategoria());
        this.setIdResponsable(prod.getIdResponsable());
        this.setObservaciones(prod.getObservaciones());
        this.setIdProductoPrincipal(prod.getIdProductoPrincipal());
    }
    public Persona sacarPersona(){
        Persona p = new Persona(this.getNombres(), this.getApellidoPaterno(), 
        this.getApellidoMaterno(),this.getTelefono(),this.getCorreo());
        return p;
    }

    public Usuario sacarUsuario(){
        Usuario u = new Usuario(this.getIdRol(), this.getUsername(), this.getPassword(), this.getFechaRegistro());
        return u;
    }


    public Integer getIdProductoPrincipal() {
        return idProductoPrincipal;
    }

    public void setIdProductoPrincipal(Integer idProductoPrincipal) {
        this.idProductoPrincipal = idProductoPrincipal;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Integer getIdResponsable() {
        return idResponsable;
    }

    public void setIdResponsable(Integer idResponsable) {
        this.idResponsable = idResponsable;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Integer getIdProductor() {
        return idProductor;
    }

    public void setIdProductor(Integer idProductor) {
        this.idProductor = idProductor;
    }

}