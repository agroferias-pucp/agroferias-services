package com.agroferias.services;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;

import com.agroferias.entities.Tienda;
import com.agroferias.entities.FeriaT;
import com.agroferias.repositories.TiendaRepository;

@Service
public class TiendaService {

    @Autowired
    private TiendaRepository tiendaRepository;

    public List<Tienda> getTiendasEmpresa(Integer idEmpresa) {
        return tiendaRepository.findAllTiendaEmpresa(idEmpresa);
    }

    public List<Tienda> getTiendasFeria(Integer idFeria) {
        return tiendaRepository.findAllTiendaFeria(idFeria);
    }

    public List<Tienda> getTiendasFeriaTipo(Integer idFeria, Integer tipoTienda) {
        return tiendaRepository.findAllTiendaFeriaTipo(idFeria, tipoTienda);
    }

    public List<FeriaT> getTiendasEmpresaApp(Integer idEmpresa) {

        List<Tienda> tiendas = tiendaRepository.findAllTiendaEmpresa(idEmpresa);
        List<FeriaT> lista = new ArrayList<FeriaT>();
        FeriaT f1 = new FeriaT(1, "Magdalena", "Calle Brasil 931");
        FeriaT f2 = new FeriaT(2, "San Borja", "Calle San Borja Norte 931");
        FeriaT f3 = new FeriaT(3, "San Miguel", "Calle Universitaria 931");
        FeriaT f;
        lista.add(f1);
        lista.add(f2);
        lista.add(f3);

        for (int i = 0; i < tiendas.size(); i++) {
            if (tiendas.get(i).getTipoTienda() == 0) {
                //marcar flag fisico de la feria con ese id
                for (int j = 0; j < lista.size(); j++) {
                    if (lista.get(j).getIdFeria() == tiendas.get(i).getIdFeria()) {
                        f = lista.get(j);
                        f.setFlagFisico(1);
                        lista.set(j, f);
                        break;
                    }
                }
            } else if (tiendas.get(i).getTipoTienda() == 1) {
                //marcar flag virtual de la feria con ese id
                for (int j = 0; j < lista.size(); j++) {
                    if (lista.get(j).getIdFeria() == tiendas.get(i).getIdFeria()) {
                        f = lista.get(j);
                        f.setFlagVirtual(1);
                        lista.set(j, f);
                        break;
                    }
                }
            }
        }

        List<FeriaT> listaFinal = new ArrayList<FeriaT>();

        for (int j = 0; j < lista.size(); j++) {
            if (lista.get(j).getFlagFisico() == 1 && lista.get(j).getFlagVirtual() == 1) {
                f = lista.get(j);
                f.setTiposTienda("Tienda Física y Virtual");
                listaFinal.add(f);
            } else if (lista.get(j).getFlagFisico() == 1 && lista.get(j).getFlagVirtual() == 0) {
                f = lista.get(j);
                f.setTiposTienda("Tienda Física");
                listaFinal.add(f);
            } else if (lista.get(j).getFlagFisico() == 0 && lista.get(j).getFlagVirtual() == 1) {
                f = lista.get(j);
                f.setTiposTienda("Tienda Virtual");
                listaFinal.add(f);
            }
        }

        return listaFinal;
    }

    public Tienda servicioPerfilTienda(Integer idTienda) {
        Tienda perfilTienda = tiendaRepository.findOne(idTienda);
        if (perfilTienda != null) {
            return perfilTienda;
        } else {
            return null;
        }
    }

    public Tienda registrarTienda(Tienda tien) {
        return tiendaRepository.save(tien);
    }

    public Tienda buscarTienda(Integer id) {
        return tiendaRepository.findByIdTienda(id);
    }

    public List<Tienda> allTiendas() {
        return tiendaRepository.findAll();
    }

    public Tienda editarTienda(Tienda nuevo) {
        Tienda tienda = tiendaRepository.findByIdTienda(nuevo.getIdTienda());
        if (tienda != null) { //si loencuentra lo modifica y retorna 1 
            tienda.setActivo(nuevo.getActivo());
            tienda.setAsignado(nuevo.getAsignado());
            tienda.setDescripcion(nuevo.getDescripcion());
            tienda.setEmpresa(nuevo.getEmpresa());
            tienda.setIdFeria(nuevo.getIdFeria());
            tienda.setIdSolicitudTienda(nuevo.getIdSolicitudTienda());
            tienda.setIdTienda(nuevo.getIdTienda());
            tienda.setNumeroPuesto(nuevo.getNumeroPuesto());
            tienda.setPosicion_x(nuevo.getPosicion_x());
            tienda.setPosicion_y(nuevo.getPosicion_y());
            tienda.setFoto(nuevo.getFoto());
            tienda.setEsPremium(nuevo.getEsPremium());
            tienda.setTipoTienda(nuevo.getTipoTienda());
            //tiendaService.registrarTienda(tienda);
            return tiendaRepository.save(tienda);
        }
        return null;
    }

    public int editarPosicionesTienda(Tienda nuevo) {
        Tienda tienda = tiendaRepository.findByIdTienda(nuevo.getIdTienda());
        if (tienda != null) { //si loencuentra lo modifica y retorna 1 
            tienda.setPosicion_x(nuevo.getPosicion_x());
            tienda.setPosicion_y(nuevo.getPosicion_y());
            //tiendaService.registrarTienda(tienda);
            return 1;
        }
        return 0;
    }

    public int eliminarTienda(int idTienda) {
        Tienda tienda = tiendaRepository.findByIdTienda(idTienda);
        if (tienda != null) {
            tienda.setActivo(0);
            tiendaRepository.save(tienda);
            return idTienda;
        } else {
            return 0;
        }
    }

}
