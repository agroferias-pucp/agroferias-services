package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Cliente")
public class Cliente{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idCliente")
    private Integer idCliente;

    @Column(name="idPersona")
    private Integer idPersona;
    @Column(name="idUsuario")
    private Integer idUsuario;
    @Column(name="DNI")
    private String dni;

    public Cliente(){

    }
    public Cliente(Integer idPersona, Integer idUsuario, String dni){
        this.idPersona=idPersona;
        this.idUsuario=idUsuario;
        this.dni=dni;
    }

    public Integer getIdCliente(){
        return this.idCliente;
    }
    public Integer getIdPersona(){
        return this.idPersona;
    }
    public Integer getIdUsuario(){
        return this.idUsuario;
    }
    public String getDni(){
        return this.dni;
    }

    public void setIdCliente(Integer idCliente){
        this.idCliente=idCliente;
    }
    public void setIdPersona(Integer idPersona){
        this.idPersona=idPersona;
    }
    public void setIdUsuario(Integer idUsuario){
        this.idUsuario=idUsuario;
    }
    public void setDni(String dni){
        this.dni=dni;
    }
}