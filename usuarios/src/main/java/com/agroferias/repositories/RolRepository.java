package com.agroferias.repositories;

import java.util.List;

import com.agroferias.entities.Rol;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RolRepository extends JpaRepository<Rol,Integer>{
    @Query(value="SELECT * FROM usuariosdb.Rol",nativeQuery = true)
    List<Rol> findAllRoles();
}