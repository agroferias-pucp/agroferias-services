package com.agroferias.entities;

import javax.persistence.*;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name = "Feria")
public class Feria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idFeria")
    private Integer idFeria;

    @Column(name = "fechaInicio", nullable = false)
    private Date fechaInicio;

    @Column(name = "fechaFin")
    private Date fechaFin;

    @Column(name = "diasAtencion", length = 15, nullable = false)
    private String diasAtencion;

    @Column(name = "direccion", length = 60, nullable = false)
    private String direccion;

    @Column(name = "horaApertura", nullable = false)
    private Time horaApertura;

    @Column(name = "horaCierre", nullable = false)
    private Time horaCierre;

    @Column(name = "logo", length = 500)
    private String logo;

    @Column(name = "nombre", length = 255, nullable = false)
    private String nombre;

    @Column(name = "fechaAperturaCaja", nullable = false)
    private Date fechaAperturaCaja;

    @Column(name = "fechaCierreCaja")
    private Date fechaCierreCaja;

    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    //@JoinColumn(name = "idEstado", nullable = false)
    private Estado estado;

    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    //@JoinColumn(name = "idDistrito", nullable = false)
    private Distrito distrito;

    public Feria() {
    }

    public Integer getIdFeria() {
        return idFeria;
    }

    public void setIdFeria(Integer idFeria) {
        this.idFeria = idFeria;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getDiasAtencion() {
        return diasAtencion;
    }

    public void setDiasAtencion(String diasAtencion) {
        this.diasAtencion = diasAtencion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Time getHoraApertura() {
        return horaApertura;
    }

    public void setHoraApertura(Time horaApertura) {
        this.horaApertura = horaApertura;
    }

    public Time getHoraCierre() {
        return horaCierre;
    }

    public void setHoraCierre(Time horaCierre) {
        this.horaCierre = horaCierre;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaAperturaCaja() {
        return fechaAperturaCaja;
    }

    public void setFechaAperturaCaja(Date fechaAperturaCaja) {
        this.fechaAperturaCaja = fechaAperturaCaja;
    }

    public Date getFechaCierreCaja() {
        return fechaCierreCaja;
    }

    public void setFechaCierreCaja(Date fechaCierreCaja) {
        this.fechaCierreCaja = fechaCierreCaja;
    }
}
