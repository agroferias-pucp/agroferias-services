package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from pedidos!";
    }

    @RequestMapping(value = "/api/pedidos")
    String pedidos_api() {
        return "hola api pedidos!";
    }

    @RequestMapping(value = "/api/pedidos/a/b")
    String pedidos_api_a_b() {
        return "hola api pedidos a b!";
    }
}