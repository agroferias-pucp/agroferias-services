FROM openjdk:8

ADD java-maven.jar /opt/java-maven/
EXPOSE 8080
WORKDIR /opt/java-maven/
CMD ["java", "-Xms512m", "-Xmx1g", "-jar", "java-maven.jar"]