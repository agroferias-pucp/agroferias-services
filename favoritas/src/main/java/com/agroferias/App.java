package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Tiendas Favoritas is alive!";
    }

    @RequestMapping(value = "/api/favoritas")
    String favoritas_api() {
        return "hola api favoritas!";
    }

    @RequestMapping(value = "/api/favoritas/a/b")
    String favoritas_api_a_b() {
        return "hola api favoritas a b!";
    }
}