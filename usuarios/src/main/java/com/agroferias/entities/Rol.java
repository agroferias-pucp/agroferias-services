package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Rol")
public class Rol{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idRol")
    private Integer idRol;

    @Column(name="descripcion")
    private String descripcion;

    public Rol(){
    }
    public Rol(Integer idRol,String desc){
        this.idRol=idRol;
        this.descripcion=desc;
    }

    public Integer getIdRol(){
        return this.idRol;
    }
    public void setIdRol(Integer id){
        this.idRol = id;
    }
    public String getDescripcion(){
        return this.descripcion;
    }
    public void setDescripcion(String desc){
        this.descripcion = desc;
    }
}