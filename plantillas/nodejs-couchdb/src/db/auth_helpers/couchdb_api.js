const bcrypt = require('bcryptjs')
const localAuth = require('./token_de_en_coders')
const log = require('debug')('users_auth_storage')

console.log('Using dbwrapper: ' + process.env.DB_WRAPPER)
console.log('Using storage ip: ' + process.env.STORAGE_IP)
console.log('Using couchdb url: ' + process.env.COUCHDB_DB_URL)
var nano = require('nano')(process.env.COUCHDB_DB_URL)
const usersDB = nano.db.use('users');

function createUser (req) {
  var username = req.body.username
  var password = req.body.password
  log('createUser()')
  const salt = bcrypt.genSaltSync()
  const hash = bcrypt.hashSync(password, salt)
  return new Promise((resolve, reject) => {
    usersDB.insert({password: hash}, username, (ko, ok) => {
      if (ko) {
        log(ko)
        reject(ko.reason)
      } else resolve(username)
    })
  })
}

function getUser (username) {
  console.log('Picking user ' + username)
  return new Promise((resolve, reject) => {
    usersDB.get(username, (ko, ok) => {
      // console.log(ko)
      // console.log(ok)
      if (ko) {
        log(ko)
        reject(ko.reason)
      } else resolve({id: ok._id, password: ok.password})
    })
  })
}

function comparePass (userPassword, databasePassword) {
  return bcrypt.compareSync(userPassword, databasePassword)
}

function ensureAuthenticated (req) {
  return new Promise((resolve, reject) => {
    var token = req.params.token
    if (!token) {
      log('user request has no token')
      reject(new Error(`No token for user ${req.params.username}`))
    }
    localAuth.decodeToken(token, (err, payload) => {
      if (err) {
        log('token has expired')
        reject(new Error(`Token for ${req.params.username} has expired`))
      }
      usersDB.get(parseInt(payload.sub, 10), (ko, ok) => {
        if (ko) {
          log(ko)
          reject(ko.reason)
        } else resolve()
      })
    })
  })
}

function removeAuthorization(username, token) {
  return new Promise((resolve, reject) => {
    if (!token) {
      log('user request has no token')
      resolve()
    }
    localAuth.decodeToken(token, (err, payload) => {
      if (err) {
        log('token has expired')
        resolve()
      }
      usersDB.destroy(parseInt(payload.sub, 10), (ko, ok) => {
        if (ko) {
          log(ko)
          reject(ko.reason)
        } else resolve()
      })
    })
  })
}

module.exports = {
  createUser,
  getUser,
  comparePass,
  ensureAuthenticated,
  removeAuthorization
}