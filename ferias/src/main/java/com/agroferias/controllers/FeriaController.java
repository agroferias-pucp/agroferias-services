package com.agroferias.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import com.agroferias.entities.Estado;
import com.agroferias.entities.Feria;
import com.agroferias.repositories.EstadoRepository;
import com.agroferias.repositories.FeriaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class FeriaController {

    @Autowired
    FeriaRepository feriaRepository;

    @Autowired
    EstadoRepository estadoRepository;

    //Crear nueva feria
    @RequestMapping(value = "/feria/registrar", method = RequestMethod.POST)
    public Feria createFeria(@RequestBody @Valid Feria newFeria){
        return feriaRepository.save(newFeria);
    }

    //Obtener información de una feria
    @RequestMapping(value = "/feria/{idFeria}", method = RequestMethod.GET)
    public Feria readFeria(@PathVariable("idFeria") Integer idFeria){
        return feriaRepository.findOne(idFeria);
    }

    //Actualizar datos de una feria
    @RequestMapping(value = "/feria/modificar/{idFeria}", method = RequestMethod.PUT)
    public Feria updateFeria(@PathVariable("idFeria") Integer idFeria,
                             @RequestBody @Valid Feria newFeria){
        Feria curFeria = feriaRepository.findByIdFeria(idFeria);
        if(curFeria.getIdFeria() != newFeria.getIdFeria()) return null;
        return feriaRepository.saveAndFlush(newFeria);
    }

    //Eliminar una feria (Finalizar una feria)
    //idEstado = 3 Finalizada
    //validar esto
    @RequestMapping(value = "/feria/eliminar/{idFeria}", method = RequestMethod.PUT)
    public Feria deleteFeria(@PathVariable("idFeria") Integer idFeria){
        Feria curFeria = feriaRepository.findByIdFeria(idFeria);
        Estado newEstado = estadoRepository.findByIdEstado(3);

        curFeria.setEstado(newEstado);
        return feriaRepository.saveAndFlush(curFeria);
    }

    //Listar todas las ferias registradas en la bd
    @RequestMapping(value = "/ferias", method = RequestMethod.GET)
    public List<Feria> listarFerias(){
        return feriaRepository.findAll();
    }

    @RequestMapping(value = "/feria/horas/{idFeria}", method = RequestMethod.GET)
    public Map<String, Object> obtenerHoras(@PathVariable("idFeria") Integer idFeria){
        Feria curFeria = new Feria();
        curFeria = feriaRepository.findByIdFeria(idFeria);
        Map<String, Object> horas = new HashMap<>();
        horas.put("horaIni", curFeria.getHoraApertura());
        horas.put("horaFin", curFeria.getHoraCierre());
        return horas;
    }
}
