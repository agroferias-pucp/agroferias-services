#!/bin/sh
# Precondiciones:
# - Conectarse a la MV via ssh anteriormente

KEYPAIR_FILE="~/dp2ec2.pem"
VM_IP="***.***.***.***"
VM_TAG="dev1"

until ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'ls /'; do sleep 3;echo "Retrying connection..."; done

# Add swap file
echo "Creating swapfile"
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo dd if=/dev/zero of=/swapfile bs=500M count=16'
echo "Configuring swapfile"
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo chmod 600 /swapfile'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo mkswap /swapfile'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo swapon /swapfile'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo swapon -s'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'echo "/swapfile swap swap defaults 0 0" | sudo tee -a /etc/fstab'

# Installing docker
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo yum update -y'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo amazon-linux-extras install docker'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo service docker start'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo usermod -a -G docker ec2-user'

# Installing gitlab-runner
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo yum install gitlab-runner -y'
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo usermod -aG docker gitlab-runner'
read -p "Enter your gitlab token: " GITLAB_TOKEN
ssh -i $KEYPAIR_FILE ec2-user@$VM_IP 'sudo gitlab-runner register --non-interactive --url https://gitlab.com/ --registration-token '$GITLAB_TOKEN' --executor shell --tag-list "ec2,'$VM_TAG'"'
