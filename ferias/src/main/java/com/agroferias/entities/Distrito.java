package com.agroferias.entities;

import javax.persistence.*;

@Entity
@Table(name = "Distrito")
public class Distrito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idDistrito")
    private Integer idDistrito;

    @Column(name = "nombre", length = 30, nullable = false)
    private String nombre;

    public Distrito() {
    }

    public Integer getIdDistrito() {
        return idDistrito;
    }

    public void setIdDistrito(Integer idDistrito) {
        this.idDistrito = idDistrito;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
