package com.agroferias.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.agroferias.entities.UnidadMedida;

@Repository
public interface UnidadMedidaRepository extends JpaRepository<UnidadMedida, Integer>{
	@Query(value = "SELECT * FROM UnidadMedida a WHERE a.activo = 1", nativeQuery = true)
	List<UnidadMedida> findAllActiveUnidadMedida();
}