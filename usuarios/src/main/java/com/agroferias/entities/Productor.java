package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Productor")
public class Productor{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idProductor")
    private Integer idProductor;

    @Column(name="idPersona")
    private Integer idPersona;
    @Column(name="idUsuario")
    private Integer idUsuario;
    @Column(name="DNI")
    private String dni;
    @Column(name="idEmpresa")
    private Integer idEmpresa;
    @Column(name="idCategoria")
    private Integer idCategoria;
    @Column(name="idResponsable")
    private Integer idResponsable;
    @Column(name="observaciones")
    private String observaciones;
    @Column(name="idProductoPrincipal")
    private Integer idProductoPrincipal;
    public Productor(){

    }
    public Productor(Integer idPersona, Integer idUsuario, String dni){
        this.idPersona=idPersona;
        this.idUsuario=idUsuario;
        this.dni=dni;
    }

    public Integer getIdProductor(){
        return this.idProductor;
    }
    public Integer getIdPersona(){
        return this.idPersona;
    }
    public Integer getIdUsuario(){
        return this.idUsuario;
    }
    public String getDni(){
        return this.dni;
    }
    public Integer getIdEmpresa(){
        return this.idEmpresa;
    }
    public Integer getIdCategoria(){
        return this.idCategoria;
    }
    public Integer getIdResponsable(){
        return this.idResponsable;
    }
    public String getObservaciones(){
        return this.observaciones;
    }
    public Integer getIdProductoPrincipal(){
        return this.idProductoPrincipal;
    }

    public void setIdProductor(Integer idProductor){
        this.idProductor=idProductor;
    }
    public void setIdPersona(Integer idPersona){
        this.idPersona=idPersona;
    }
    public void setIdUsuario(Integer idUsuario){
        this.idUsuario=idUsuario;
    }
    public void setDni(String dni){
        this.dni=dni;
    }

    public void setIdEmpresa(Integer idEmpresa){
        this.idEmpresa=idEmpresa;
    }
    public void setIdCategoria(Integer idCategoria){
        this.idCategoria=idCategoria;
    }
    public void setIdResponsable(Integer idResponsable){
        this.idResponsable=idResponsable;
    }
    public void setObservaciones(String observaciones){
        this.observaciones=observaciones;
    }
    public void setIdProductoPrincipal(Integer idProductoPrincipal){
        this.idProductoPrincipal=idProductoPrincipal;
    }

}