package com.agroferias.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Empresa")
public class Empresa {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEmpresa")
	private Integer idEmpresa;

    @Column(name = "idSolicitudEmpresa")	
	private Integer idSolicitudEmpresa;

    @Column(name = "razonSocial")	
	private String razonSocial;

    @Column(name = "rucRus")	
	private String rucRus;

    @Column(name = "tipoPersona")	
	private Integer tipoPersona;

    @Column(name = "nombreComercial")	
	private String nombreComercial;

    @Column(name = "direccion")	
	private String direccion;

    @Column(name = "idCategoria")	
	private Integer idCategoria;

    @Column(name = "idDepartamento")	
	private Integer idDepartamento;

    @Column(name = "telefono")	
	private String telefono;

    @Column(name = "celular")	
	private String celular;

    @Column(name = "email")	
	private String email;

    @Column(name = "direccionWeb")	
	private String direccionWeb;

    @Column(name = "fechaRegistro")	
	private Date fechaRegistro;

    @Column(name = "facebook")	
	private String facebook;

    @Column(name = "instagram")	
	private String instagram;

    @Column(name = "twitter")	
	private String twitter;

    @Column(name = "activo")	
	private Integer activo;


    public Empresa() {
    }

    public Empresa(Integer idEmpresa, Integer idSolicitudEmpresa, String razonSocial, String rucRus, Integer tipoPersona, String nombreComercial, String direccion, Integer idCategoria, Integer idDepartamento, String telefono, String celular, String email, String direccionWeb, Date fechaRegistro, String facebook, String instagram, String twitter, Integer activo) {
        this.idEmpresa = idEmpresa;
        this.idSolicitudEmpresa = idSolicitudEmpresa;
        this.razonSocial = razonSocial;
        this.rucRus = rucRus;
        this.tipoPersona = tipoPersona;
        this.nombreComercial = nombreComercial;
        this.direccion = direccion;
        this.idCategoria = idCategoria;
        this.idDepartamento = idDepartamento;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
        this.direccionWeb = direccionWeb;
        this.fechaRegistro = fechaRegistro;
        this.facebook = facebook;
        this.instagram = instagram;
        this.twitter = twitter;
        this.activo = activo;
    }

    public Integer getIdEmpresa() {
        return this.idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdSolicitudEmpresa() {
        return this.idSolicitudEmpresa;
    }

    public void setIdSolicitudEmpresa(Integer idSolicitudEmpresa) {
        this.idSolicitudEmpresa = idSolicitudEmpresa;
    }

    public String getRazonSocial() {
        return this.razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getRucRus() {
        return this.rucRus;
    }

    public void setRucRus(String rucRus) {
        this.rucRus = rucRus;
    }

    public Integer getTipoPersona() {
        return this.tipoPersona;
    }

    public void setTipoPersona(Integer tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getNombreComercial() {
        return this.nombreComercial;
    }

    public void setNombreComercial(String nombreComercial) {
        this.nombreComercial = nombreComercial;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Integer getIdCategoria() {
        return this.idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdDepartamento() {
        return this.idDepartamento;
    }

    public void setIdDepartamento(Integer idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return this.celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDireccionWeb() {
        return this.direccionWeb;
    }

    public void setDireccionWeb(String direccionWeb) {
        this.direccionWeb = direccionWeb;
    }

    public Date getFechaRegistro() {
        return this.fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFacebook() {
        return this.facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getInstagram() {
        return this.instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return this.twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public Integer getActivo() {
        return this.activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
   
}