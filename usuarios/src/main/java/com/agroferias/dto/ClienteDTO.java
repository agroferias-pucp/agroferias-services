package com.agroferias.dto;

import java.util.Date;

import com.agroferias.entities.Cliente;
import com.agroferias.entities.Persona;
import com.agroferias.entities.Usuario;

public class ClienteDTO {
    private Integer idCliente;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private String correo;
    private Integer idRol;
    private String username;
    private String password;
    private Date fechaRegistro;
    private String dni;

    public ClienteDTO() {

    }
    public ClienteDTO(Cliente cli, Persona per, Usuario u){
        this.setIdCliente(cli.getIdCliente());
        this.setNombres(per.getNombres());
        this.setApellidoPaterno(per.getApellidoPaterno());
        this.setApellidoMaterno(per.getApellidoMaterno());
        this.setTelefono(per.getTelefono());
        this.setCorreo(per.getCorreo());
        this.setIdRol(u.getIdRol());
        this.setUsername(u.getUsername());
        this.setPassword(u.getPassword());
        this.setFechaRegistro(u.getFechaRegistro());
        this.setDni(cli.getDni());
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }
}