package com.agroferias.controller;


import com.agroferias.entities.MapImageFile;
import com.agroferias.repositories.MapImageFileRepository;
import org.hibernate.SQLQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/mapa/")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class MapImageFileController {
    @Autowired
    private MapImageFileRepository mapImageFileRepository;

    @RequestMapping(value = "{idFeria}/imagen", method = RequestMethod.POST)
    public MapImageFile newImage(@RequestBody String url, @PathVariable("idFeria") Integer idFeria){
        MapImageFile mapImageFileNEW = new MapImageFile();

        mapImageFileNEW.setIdFeria(idFeria);
        mapImageFileNEW.setUrlImagen(url);

        return mapImageFileRepository.save(mapImageFileNEW);
    }

    @RequestMapping(value = "{idFeria}/imagen", method = RequestMethod.PUT)
    public MapImageFile updateImage(@RequestBody String url, @PathVariable("idFeria") Integer idFeria){
        MapImageFile mapImageFile =  mapImageFileRepository.findByIdFeria(idFeria);
        mapImageFile.setUrlImagen(url);
        return mapImageFileRepository.saveAndFlush(mapImageFile);
    }

    @RequestMapping(value = "{idFeria}/imagen", method = RequestMethod.GET)
    public MapImageFile getImageMapInfo(@PathVariable("idFeria") Integer idFeria){
        return mapImageFileRepository.findByIdFeria(idFeria);
    }
}
