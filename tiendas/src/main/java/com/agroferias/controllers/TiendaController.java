package com.agroferias.controllers;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import com.agroferias.entities.Tienda;
import com.agroferias.entities.FeriaT;
import com.agroferias.services.TiendaService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class TiendaController {
    
    @Autowired
    private TiendaService tiendaService;

    @RequestMapping(value = "/api/tiendas/empresa/{idEmpresa}", method = RequestMethod.GET)
    public List<Tienda> tiendasEmpresa(@PathVariable(value = "idEmpresa") Integer idEmpresa) {
        List<Tienda> lista = tiendaService.getTiendasEmpresa(idEmpresa);
        return lista;
    }

    @RequestMapping(value = "/api/tiendas/feria/{idFeria}", method = RequestMethod.GET)
    public List<Tienda> tiendasFeria(@PathVariable(value = "idFeria") Integer idFeria) {
        List<Tienda> lista = tiendaService.getTiendasFeria(idFeria);
        return lista;
    }

    @RequestMapping(value = "/api/tiendas/feria/fisica/{idFeria}", method = RequestMethod.GET)
    public List<Tienda> tiendasFeriaFisico(@PathVariable(value = "idFeria") Integer idFeria) {
        List<Tienda> lista = tiendaService.getTiendasFeriaTipo(idFeria, 0);
        return lista;
    }

    @RequestMapping(value = "/api/tiendas/feria/virtual/{idFeria}", method = RequestMethod.GET)
    public List<Tienda> tiendasFeriaVirtual(@PathVariable(value = "idFeria") Integer idFeria) {
        List<Tienda> lista = tiendaService.getTiendasFeriaTipo(idFeria, 1);
        return lista;
    }

    @RequestMapping(value = "/api/tiendas/empresa/app/{idEmpresa}", method = RequestMethod.GET)
    public List<FeriaT> tiendasEmpresaApp(@PathVariable(value = "idEmpresa") Integer idEmpresa) {
        List<FeriaT> lista = tiendaService.getTiendasEmpresaApp(idEmpresa);
        return lista;
    }
    
    @RequestMapping(value = "/api/tienda/perfil/{idTienda}", method = RequestMethod.GET)
    public Tienda perfilTienda(@PathVariable(value = "idTienda") int idTienda) {
        Tienda tienda = tiendaService.servicioPerfilTienda(idTienda);
        if (tienda == null) {
            return null;
        }
        return tienda;
    }
    
    @RequestMapping(value = "/api/tienda/registrar", method = RequestMethod.POST,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    Tienda crear(@RequestBody Tienda tienda) {
        tiendaService.registrarTienda(tienda);
        return tienda;
    }
    
    @RequestMapping(value = "/api/tiendas/all")
    public List<Tienda> allEmpresas() {
        return tiendaService.allTiendas();
    }
    
    @RequestMapping(value = "/api/tienda/eliminar", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Integer eliminarTienda(@RequestParam(value = "idEliminar") int idEliminar) throws Exception {
        return tiendaService.eliminarTienda(idEliminar);
    }
    
    @RequestMapping(value = "/api/tienda/modificar", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Tienda editar(@RequestBody Tienda nuevo) throws Exception {
        return tiendaService.editarTienda(nuevo);
    }
    
    @RequestMapping(value = "/api/tienda/mapa", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Integer modificarPosiciones(@RequestBody Tienda nuevo) throws Exception {
        return tiendaService.editarPosicionesTienda(nuevo);
    }

//    @RequestMapping(method = RequestMethod.POST, value = "/api/tienda/mapa/{idFeria}")
//    public @ResponseBody
//    Integer modificarPosicionesXY(@RequestParam int idFeria,@RequestParam int x, @RequestParam int y) {
//        FeriaT feria  = 
//
//    }
}
