package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @RequestMapping(value = "/")
    String hello() {
        return "Hello World from pagos!";
    }

    @RequestMapping(value = "/api/pagos")
    String pagos_api() {
        return "hola api pagos!";
    }

    @RequestMapping(value = "/api/pagos/a/b")
    String pagos_api_a_b() {
        return "hola api pagos a b!";
    }
}