import http from 'http';
import express from 'express';
import cors from 'cors';
import logger from 'morgan';
import bodyParser from 'body-parser';

import initializeDb from './db';
import config from './config.json';
import app from './app';


const log = require('debug')('server')

const server = express()
server.use(logger('dev'))
server.use(bodyParser.json())
server.use(bodyParser.urlencoded({ extended: false }))
server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  next()
})

server.use('/', app)

server.use((req, res, next) => {
	const err = new Error('Not Found')
	err.status = 404
	next(err)
})

server.use((err, req, res, next) => {
	const message = req.app.get('env') === 'development' ? err : {}
	log(`${message}`)
	log(err)
	res.status(err.status || 500)
	res.json({
		status: 'error'
	})
})

function normalizePort (val) {
	const port = parseInt(val, 10)
	if (isNaN(port)) { return val }
	if (port >= 0) { return port }
	return false
}

const port = normalizePort(process.env.PORT || config.port)
server.listen(port)
log(`Listening at port ${port}`)

export default server;
