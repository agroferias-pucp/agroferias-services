#!/bin/bash

IMG_MAVEN_BUILDER="maven:3"


function init_maven_builder()
{
    printf "y\n"  | docker container prune
    if [ ! "$(docker container ls -q --filter name=maven-builder)" ]; then
        docker run -it -d --name=maven-builder $IMG_MAVEN_BUILDER bash;
        echo "Waiting maven builder container...";
        until [ "$(docker container ls -q --filter name=maven-builder)" ]; do sleep 1; done;
        BUILDER=$(docker container ls -q --filter name=maven-builder);
        docker exec $BUILDER apt-get update;
        docker exec $BUILDER apt-get install -y rsync;
    else echo "Maven builder is ready."; fi
}

function build_maven_project()
{
    # BUILDER=$(docker container ls -q --filter name=maven-builder)
    # docker cp "./$1" $BUILDER:/
    # docker exec $BUILDER mkdir -p "/app/$1"
    # docker exec $BUILDER rsync -a --delete --filter=':- .gitignore' "/$1/" "/app/$1"
    # docker exec $BUILDER rm -rf "/$1"
    # docker exec $BUILDER bash -c "cd /app/$1 && mvn -q clean package"
    # docker cp $BUILDER:/app/$1/target ./$1;
    cd "./$1"
    rm -rf target
    mkdir target
    mkdir -p ~/.m2
    chown -R $UID ~/.m2
    docker run -v ~/.m2:/var/maven/.m2 --rm -u $UID -v "$PWD":/usr/src/mymaven -v "$PWD/target:/usr/src/mymaven/target" -w /usr/src/mymaven maven mvn -Duser.home=/var/maven package
    cd ..
}

function create_maven_image()
{
    build_maven_project $1
    cd "$1/"
}

function build_all_java() {
    cd ../../
    build_maven_project test-java-maven
    cd test-java-maven/target
    docker build -t test-java-maven:latest .
}