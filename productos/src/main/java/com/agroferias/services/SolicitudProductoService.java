package com.agroferias.services;

import com.agroferias.entities.SolicitudProducto;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.agroferias.repositories.SolicitudProductoRepository;

@Service
public class SolicitudProductoService {
    
    @Autowired
    private SolicitudProductoRepository solicitudProductoRepository;

    public List<SolicitudProducto> getSolicitudProductoFeria(Integer idFeria){
        return solicitudProductoRepository.findByIdFeria(idFeria);
    }

    public List<SolicitudProducto> getSolicitudProductoTienda(Integer idTienda){
        return solicitudProductoRepository.findByIdTienda(idTienda);
    }

    public SolicitudProducto insert (SolicitudProducto solicitudProducto){
        return solicitudProductoRepository.save(solicitudProducto);
    }

    public Integer aceptarSolicitudProducto(Integer idSolicitud){
        Optional<SolicitudProducto> optional = solicitudProductoRepository.findByIdSolicitud(idSolicitud);
		if (optional.isPresent()) {
            SolicitudProducto a = optional.get();
            a.setEstado(2);
            solicitudProductoRepository.save(a);
            return idSolicitud;
		}
		return null;
    }

    public Integer rechazarSolicitudProducto(Integer idSolicitud){
        Optional<SolicitudProducto> optional = solicitudProductoRepository.findByIdSolicitud(idSolicitud);
		if (optional.isPresent()) {
            SolicitudProducto a = optional.get();
            a.setEstado(3);
			solicitudProductoRepository.save(a);
            return idSolicitud;
		}
		return null;
    }
}
