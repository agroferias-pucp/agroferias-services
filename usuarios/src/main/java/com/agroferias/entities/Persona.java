package com.agroferias.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="Persona")
public class Persona{
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idPersona")
    private Integer idPersona;
    @Column(name="nombres")
    private String nombres;
    @Column(name="apellidoPaterno")
    private String apellidoPaterno;
    @Column(name="apellidoMaterno")
    private String apellidoMaterno;
    @Column(name="telefono")
    private String telefono;
    @Column(name="correo")
    private String correo;

    public Persona(){

    }
    public Persona(String nombres, String apellidoP, String apellidoM,String telef,String correo){
        this.nombres=nombres;
        this.apellidoPaterno=apellidoP;
        this.apellidoMaterno=apellidoM;
        this.telefono=telef;
        this.correo=correo;
    }
    public Integer getIdPersona(){
        return this.idPersona;
    }
    public String getNombres(){
        return this.nombres;
    }
    public String getApellidoPaterno(){
        return this.apellidoPaterno;
    }
    public String getApellidoMaterno(){
        return this.apellidoMaterno;
    }
    public String getTelefono(){
        return this.telefono;
    }
    public String getCorreo(){
        return this.correo;
    }
    public void setNombres(String nombres){
        this.nombres=nombres;
    }
    public void setApellidoPaterno(String apellidoP){
        this.apellidoPaterno=apellidoP;
    }
    public void setApellidoMaterno(String apellidoM){
        this.apellidoMaterno=apellidoM;
    }
    public void setTelefono(String telef){
        this.telefono=telef;
    }
    public void setCorreo(String correo){
        this.correo=correo;
    }
}