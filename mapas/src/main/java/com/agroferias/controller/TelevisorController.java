package com.agroferias.controller;

import com.agroferias.entities.Televisor;
import com.agroferias.repositories.TelevisorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

/*
https://docs.google.com/spreadsheets/d/1_CnH37n5VMPYuYylfyYdsFRdaU76qebBiUDamYjCn1I/edit#gid=0
* */
@RestController
@RequestMapping("/api/mapa/")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class TelevisorController {
    @Autowired
    TelevisorRepository televisorRepository;

    @RequestMapping(value = "{idFeria}/televisor",method = RequestMethod.GET)
    public List<Televisor> getAllTelevisores(@PathVariable("idFeria") Integer idFeria){
        return televisorRepository.findAllByIdFeria(idFeria);
    }

    //Falta validar
    @RequestMapping(value = "{idFeria}/televisor/{idTelevisor}",method = RequestMethod.PUT)
    public Televisor updateTelevisor(@PathVariable("idFeria") Integer idFeria ,
                                    @PathVariable("idTelevisor") Integer idTelevisor ,
                                    @RequestBody @Valid Televisor newTelevisor){
        Televisor old_televisor = televisorRepository.findByIdTelevisorAndIdFeria(idTelevisor,idFeria);
        if (old_televisor.getIdFeria() != newTelevisor.getIdFeria()) return null;
        if (old_televisor.getIdTelevisor() != newTelevisor.getIdTelevisor()) return null;
        newTelevisor = televisorRepository.saveAndFlush(newTelevisor);
        return newTelevisor;
    }

    //Nuevo televisor
    @RequestMapping(value = "{idFeria}/televisor",method = RequestMethod.POST)
    public Televisor createTelevisor(@PathVariable("idFeria") Integer idFeria,
                                     @RequestBody @Valid Televisor newTelevisor){
        return televisorRepository.save(newTelevisor);
    }

    //Obtener informacion de un televisor
    //Falta validar
    @RequestMapping(value = "{idFeria}/televisor/{idTelevisor}",method = RequestMethod.GET)
    public Televisor getOneTelevisor(@PathVariable("idFeria") Integer idFeria ,
                                    @PathVariable("idTelevisor") Integer idTelevisor){
        return televisorRepository.findByIdTelevisorAndIdFeria(idTelevisor , idFeria);
    }

}
