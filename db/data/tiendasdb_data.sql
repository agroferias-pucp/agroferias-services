
use tiendasdb;

INSERT INTO SolicitudEmpresa(idSolicitudEmpresa,idGestor,fase,razonSocial,tipoPersona,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,tipoParticipacion,estadoSolictud,idFeria,idProductor,descProductos,encargadoSemanal,fechaRegistro,fechaCita,fechaModificacion,idUsuarioModifica) VALUES (1,NULL,1,'Vacas Lecheras EIRL',2,'Vacas Lecheras','Av. del Aire 765, San Miguel, Lima',1,1,'01-4561236','987654321','vacaslecheras@gmail.com','https://es.wikipedia.org/wiki/Razas_de_vacas_lecheras',1,'APR',1,NULL,NULL,NULL,'06/10/2019','07/10/2019',NULL,NULL);
INSERT INTO SolicitudEmpresa(idSolicitudEmpresa,idGestor,fase,razonSocial,tipoPersona,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,tipoParticipacion,estadoSolictud,idFeria,idProductor,descProductos,encargadoSemanal,fechaRegistro,fechaCita,fechaModificacion,idUsuarioModifica) VALUES (2,NULL,1,'Frutas Fructiferas SAC',2,'Frutas Fructiferas','Jr. Las Frutas 123, Miraflores, Lima',2,1,'01-3216543','963852741','frutas@gmail.com','http://blog.agrologica.es/formaciones-vegetativas-y-fructiferas-en-poda-frutales-almendro-cerezo-ciruelo-melocotonero-albaricoquero-peral-manzano/',1,'APR',2,NULL,NULL,NULL,'06/10/2019','08/10/2019',NULL,NULL);
INSERT INTO SolicitudEmpresa(idSolicitudEmpresa,idGestor,fase,razonSocial,tipoPersona,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,tipoParticipacion,estadoSolictud,idFeria,idProductor,descProductos,encargadoSemanal,fechaRegistro,fechaCita,fechaModificacion,idUsuarioModifica) VALUES (3,NULL,1,'Carnes Yumi SAC',2,'Carnes Yumi','Calle Doña Vaca 345, Lince, Lima',1,1,'01-6325465','951623847','carnesyumi@hotmail.com','https://es-la.facebook.com/carne.comida.9',0,'APR',3,NULL,NULL,NULL,'06/10/2019','09/10/2019',NULL,NULL);
INSERT INTO SolicitudEmpresa(idSolicitudEmpresa,idGestor,fase,razonSocial,tipoPersona,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,tipoParticipacion,estadoSolictud,idFeria,idProductor,descProductos,encargadoSemanal,fechaRegistro,fechaCita,fechaModificacion,idUsuarioModifica) VALUES (4,NULL,1,'Asociación de Trabajadores de Huancavelica SAC',0,'Huancavelicious','Calle los Cedros 269, Huancavelica, Huancavelica',3,9,'067-451149','912345678','huancas@yahoo.es','https://www.pinterest.es/pin/479703797791307584/',1,'APR',4,NULL,NULL,NULL,'06/10/2019','10/10/2019',NULL,NULL);


INSERT INTO Empresa(idEmpresa,idSolicitudEmpresa,razonSocial,tipoPersona,rucRus,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,fechaRegistro,facebook,instagram,twitter,activo) VALUES (1,1,'Vacas Lecheras EIRL',2,'21234567890','Vacas Lecheras','Av. del Aire 765, San Miguel, Lima',1,1,'01-4561236','987654321','vacaslecheras@gmail.com','https://es.wikipedia.org/wiki/Razas_de_vacas_lecheras','03/10/2019','https://es-la.facebook.com/vacaslecherasmundial/','https://www.instagram.com/p/BqLgKB0HcY5/?hl=es',NULL,1);
INSERT INTO Empresa(idEmpresa,idSolicitudEmpresa,razonSocial,tipoPersona,rucRus,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,fechaRegistro,facebook,instagram,twitter,activo) VALUES (2,2,'Frutas Fructiferas SAC',2,'20987654321','Frutas Fructiferas','Jr. Las Frutas 123, Miraflores, Lima',2,1,'01-3216543','963852741','frutas@gmail.com','http://blog.agrologica.es/formaciones-vegetativas-y-fructiferas-en-poda-frutales-almendro-cerezo-ciruelo-melocotonero-albaricoquero-peral-manzano/','03/10/2019',NULL,NULL,NULL,1);
INSERT INTO Empresa(idEmpresa,idSolicitudEmpresa,razonSocial,tipoPersona,rucRus,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,fechaRegistro,facebook,instagram,twitter,activo) VALUES (3,3,'Carnes Yumi SAC',2,'20147258369','Carnes Yumi','Calle Doña Vaca 345, Lince, Lima',1,1,'01-6325465','951623847','carnesyumi@hotmail.com',NULL,'03/10/2019','https://es-la.facebook.com/carne.comida.9',NULL,NULL,1);
INSERT INTO Empresa(idEmpresa,idSolicitudEmpresa,razonSocial,tipoPersona,rucRus,nombreComercial,direccion,idCategoria,idDepartamento,telefono,celular,email,direccionWeb,fechaRegistro,facebook,instagram,twitter,activo) VALUES (4,4,'Asociación de Trabajadores de Huancavelica SAC',0,'20963852741','Huancavelicious','Calle los Cedros 269, Huancavelica, Huancavelica',3,9,'067-451149','912345678','huancas@yahoo.es','https://www.pinterest.es/pin/479703797791307584/','02/10/2019',NULL,NULL,NULL,1);


INSERT INTO Departamento(idDepartamento,nombre) VALUES (1,'Amazonas');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (2,'Áncash');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (3,'Apurímac');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (4,'Arequipa');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (5,'Ayacucho');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (6,'Cajamarca');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (7,'Callao');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (8,'Cusco');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (9,'Huancavelica');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (10,'Huánuco');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (11,'Ica');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (12,'Junín');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (13,'La Libertad');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (14,'Lambayeque');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (15,'Lima');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (16,'Loreto');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (17,'Madre de Dios');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (18,'Moquegua');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (19,'Pasco');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (20,'Piura');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (21,'Puno');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (22,'San Martín');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (23,'Tacna');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (24,'Tumbes');
INSERT INTO Departamento(idDepartamento,nombre) VALUES (25,'Ucayali');


INSERT INTO Tienda(idTienda,idFeria,idEmpresa,numeroPuesto,descripcion,idSolicitudTienda,tipoTienda,posicion_x,posicion_y,asignado,foto,activo,esPremium) VALUES (1,1,1,'1','Nuestra tienda se dedica a la producción de lácteos tales como leches, quesos y Yogurts',1,1,5,5,1,NULL,1,0);
INSERT INTO Tienda(idTienda,idFeria,idEmpresa,numeroPuesto,descripcion,idSolicitudTienda,tipoTienda,posicion_x,posicion_y,asignado,foto,activo,esPremium) VALUES (2,1,2,'2','NO USAMOS PESTICIDAS! Ofrecemos frutas 100% orgánicas',2,1,5,7,1,NULL,1,0);
INSERT INTO Tienda(idTienda,idFeria,idEmpresa,numeroPuesto,descripcion,idSolicitudTienda,tipoTienda,posicion_x,posicion_y,asignado,foto,activo,esPremium) VALUES (3,2,3,'3','Los mejores cortes los encontrarás aquí',3,1,7,5,1,NULL,1,0);
INSERT INTO Tienda(idTienda,idFeria,idEmpresa,numeroPuesto,descripcion,idSolicitudTienda,tipoTienda,posicion_x,posicion_y,asignado,foto,activo,esPremium) VALUES (4,2,4,'4','Recien llegado de huancavélica, decenas de especies y enlatados',4,1,7,7,1,NULL,1,1);



