package com.agroferias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    // Este método es necesario para que el servicio pueda ser detectado.
    @RequestMapping(value = "/")
    String healthcheck() {
        return "Tiendas is alive!";
    }

    /*@RequestMapping(value = "/api/tiendas")
    String tiendas_api() {
        return "hola api tiendas!";
    }*/

    @RequestMapping(value = "/api/tiendas/a/b")
    String tiendas_api_a_b() {
        return "hola api tiendas a b!";
    }
}