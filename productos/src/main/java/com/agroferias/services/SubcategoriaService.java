package com.agroferias.services;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import com.agroferias.entities.Subcategoria;
import com.agroferias.repositories.SubcategoriaRepository;

@Service
public class SubcategoriaService {

	@Autowired
	private SubcategoriaRepository subcategoriaRepository;

	public List<Subcategoria> getActiveSubcategoria() {
		return subcategoriaRepository.findAllActiveSubcategoria();
	}

	public List<Subcategoria> getSubcategoriaCategoria(Integer idCategoria) {
		return subcategoriaRepository.findAllActiveSubcategoriaCategoria(idCategoria);
	}

	public Subcategoria insert(Subcategoria subcategoria) {
		return subcategoriaRepository.save(subcategoria);
	}

	public Subcategoria update(Subcategoria subcategoria) {

		Optional<Subcategoria> optional = subcategoriaRepository.findByIdSubcategoria(subcategoria.getIdSubcategoria());
		if (optional.isPresent()) {
			Subcategoria a = optional.get();
			a.setIdCategoria(subcategoria.getIdCategoria());
			a.setNombre(subcategoria.getNombre());
			a.setDescripcion(subcategoria.getDescripcion());
			a.setActivo(subcategoria.getActivo());
			return subcategoriaRepository.save(a);
		}
		return null;

	}

	public Subcategoria delete(Subcategoria subcategoria) {

		Optional<Subcategoria> optional = subcategoriaRepository.findByIdSubcategoria(subcategoria.getIdSubcategoria());
		if (optional.isPresent()) {
			Subcategoria a = optional.get();
			a.setActivo(0);
			return subcategoriaRepository.save(a);
		}
		return null;

	}

}