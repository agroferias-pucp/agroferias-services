package com.agroferias.entities;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MapImageFile")
public class MapImageFile implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idImageFile;

    @Column(name = "urlImagen")
    private String urlImagen;

    @Column(name = "idferia")
    private Integer idFeria;


    public MapImageFile() {
    }

    public MapImageFile(String urlImagen, Integer idFeria) {
        this.urlImagen = urlImagen;
        this.idFeria = idFeria;
    }

    public Integer getIdImageFile() {
        return idImageFile;
    }

    public void setIdImageFile(Integer idImageFile) {
        this.idImageFile = idImageFile;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public Integer getIdFeria() {
        return idFeria;
    }

    public void setIdFeria(Integer idFeria) {
        this.idFeria = idFeria;
    }
}
