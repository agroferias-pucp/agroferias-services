package com.agroferias.repositories;

import com.agroferias.entities.MapImageFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MapImageFileRepository extends JpaRepository<MapImageFile , String> {
    MapImageFile findByIdFeria(Integer idFeria);
}
