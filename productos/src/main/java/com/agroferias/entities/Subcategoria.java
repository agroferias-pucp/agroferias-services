package com.agroferias.entities;

import javax.persistence.*;

@Entity
@Table(name = "Subcategoria")
public class Subcategoria {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSubcategoria")
    private Integer idSubcategoria;
    
    @ManyToOne
    @JoinColumn(name = "idCategoria")
    private Categoria categoria;

    @Column(name = "nombre")	
	private String nombre;

    @Lob
    @Column(name = "descripcion", length=1024)	
	private String descripcion;

    @Column(name = "activo")	
	private Integer activo;

    public Subcategoria() {
    }

    public Subcategoria(Integer idSubcategoria, Categoria categoria, String nombre, String descripcion, Integer activo) {
        super();
        this.idSubcategoria = idSubcategoria;
        this.categoria = categoria;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.activo = activo;
    }

    public Subcategoria(Categoria categoria, String nombre, String descripcion, Integer activo) {
        super();
        this.categoria = categoria;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.activo = activo;
    }

    public Integer getIdSubcategoria() {
        return this.idSubcategoria;
    }

    public void setIdSubcategoria(Integer idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    public Categoria getIdCategoria() {
        return this.categoria;
    }

    public void setIdCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getActivo() {
        return this.activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }

}

