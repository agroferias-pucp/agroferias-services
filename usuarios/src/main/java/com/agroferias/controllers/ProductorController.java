package com.agroferias.controllers;

import java.util.List;

import com.agroferias.dto.AutenticationDTO;
import com.agroferias.dto.ProductorDTO;
import com.agroferias.services.ProductorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/usuario/productor")
@CrossOrigin(origins="*",methods={RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class ProductorController{
    @Autowired
    private ProductorService servicioProductor = new ProductorService();

    @RequestMapping(value="/registro",method=RequestMethod.POST)
    public ProductorDTO registrarProductor(@RequestBody ProductorDTO productorRecibido){
        productorRecibido = this.servicioProductor.registrarProductor(productorRecibido);
        return productorRecibido;
    }
    @RequestMapping(value="/autenticacion",method=RequestMethod.POST)
    public Integer validarProductor(@RequestBody AutenticationDTO credenciales){
        //validar usuario
        return 1;
    }   
    @RequestMapping(value="/{idProductor}")
    public ProductorDTO getProductorDTO(@PathVariable("idProductor") int id){
        ProductorDTO p = this.servicioProductor.getProductorDTO(id);
        return p;
    }
    @RequestMapping(value="/eliminar/{idProductor}")
    public Integer eliminarProductor(@PathVariable("idProductor") int id){
        this.servicioProductor.eliminarProductor(id);
        return id;
    }
    @RequestMapping(value="/listar")
    public List<ProductorDTO> listarProductores(){
        return this.servicioProductor.getProductores();
    }
}